//! Generic video recording from a typical camera
#include <iostream>
#include <string>
#include <strstream>

// OpenCV lib
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/tracking_events.h"
#include "sense/videocapture_dispatch.h"

static std::string WINDOW_NAME = "Cam Recorder";

using namespace std;
using namespace cv;
using namespace sense;

int main(int argc, char ** argv) {
  const int id = (argc >= 2) ? std::atoi(argv[1]) : 0;
  const std::string path = (argc >= 3) ? argv[2] : "./";
  VideoCaptureDispatch videodispatch(id);
  videodispatch.SetProperty("Brightness", 50);
  videodispatch.SetProperty("Exposure", 40);
  videodispatch.SetProperty("Gain", 40);

  sense::SenseSubscriber video_rec;
  video_rec.Register(videodispatch);

  cv::namedWindow(WINDOW_NAME, CV_WINDOW_AUTOSIZE);
  cv::Mat cur_frame, m_frame_bgr;
  bool stop = false;
  bool pausing = false;
  char key = '0';
  int grab_count = 0;
  bool store = false;

  while (!stop) {
    if (!video_rec.empty() && !pausing) {
      auto evt = std::dynamic_pointer_cast<ImageEvent>(video_rec.pop());
      cur_frame = evt->frame();
#if defined(WIN32) && defined(FLIPPED_IMAGE)
      cv::flip(cur_frame, cur_frame, 0);
#elif defined(BAYER_IMAGE)
      cvtColor(cur_frame, cur_frame, CV_BayerRG2RGB);
#endif
      cv::cvtColor(cur_frame, cur_frame, CV_RGB2BGR);

      if (key == 'g' || store) {
        char ca_temp[100];
        sprintf(ca_temp, "image-%d.tiff", grab_count++);
        cout << "saving image as " << ca_temp << endl;
        cv::imwrite(ca_temp, cur_frame);
      }
    }

    if (!cur_frame.empty())
      cv::imshow(WINDOW_NAME, cur_frame);

    //  Key press handling
    key = cv::waitKey(20);
    switch (key) {
    case 27:
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      break;
    case 'g':
      break;
    case 's':
      store = !store;
      break;
    }
  }  // end loop
  return 0;
}  // end main()