// OpenCV based video recording from a typical camera
#include <iostream>
#include <memory>
#include <string>

// OpenCV lib
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/generic_capture.h"
#include "sense/tracking_events.h"

static std::string WINDOW_NAME = "Generic Camera Recorder";

#define ESCAPE 27  // ASCII code for the escape key.

using namespace sense;
using namespace std;
using namespace cv;

int main(int argc, char ** argv) {
  const int id = (argc == 2) ? std::atoi(argv[1]) : 0;
  sense::GenericCapture vc_cam(id);
  sense::SenseSubscriber video_rec;
  video_rec.Register(vc_cam);

  cv::namedWindow(WINDOW_NAME, CV_WINDOW_AUTOSIZE);
  cv::Mat cur_frame;
  bool stop = false;
  bool pausing = false;
  char key = 0;
  int grab_count = 0;
  bool store = false;

  while (!stop) {
    if (!video_rec.empty() && !pausing) {
      auto evt = std::dynamic_pointer_cast<ImageEvent>(video_rec.pop());
      cur_frame = evt->frame();
#if defined(WIN32) && defined(FLIPPED_IMAGE)
      cv::flip(cur_frame, cur_frame, 0);
#elif defined(BAYER_IMAGE)
      cvtColor(cur_frame, cur_frame, CV_BayerRG2RGB);
#endif
      cv::cvtColor(cur_frame, cur_frame, CV_RGB2BGR);

      if (key == 'g' || store) {
        char ca_temp[100];
        sprintf(ca_temp, "image-%d.tiff", grab_count++);
        cout << "saving image as " << ca_temp << endl;
        cv::imwrite(ca_temp, cur_frame);
      }
    }

    if (!cur_frame.empty())
      cv::imshow(WINDOW_NAME, cur_frame);

    //  Key press handling
    key = cv::waitKey(20);
    switch (key) {
    case ESCAPE:
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      break;
    case 'g':
      break;
    case 's':
      store = !store;
      break;
    }
  }  // end loop
  vc_cam.release();
  return 0;
}  // end main()