#include "sense/videocapture_dispatch.h"
#include <array>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/tracking_events.h"

using namespace sense;

static const char* WINDOW_NAME = "Generic Camera Controller";
static VideoCaptureDispatch videodispatch(0);
static GenericParameter param;
static int brightness, expose, gain;

void set_trackbar_value(const std::string trackbarname, 
                     const std::string reftext) {
  std::pair<unsigned int, unsigned int> minmax = param.map_paramid_minmax[reftext];
  if (minmax.second > 0) {
    unsigned int value = param.map_string_paramvalue[reftext];
    value = 100 * (value - minmax.first) / (minmax.second - minmax.first) - 1;

    cv::setTrackbarPos(trackbarname.c_str(), WINDOW_NAME, value);
  }
  param.print_parameter(reftext);
}

void on_common_trackbar(std::string bar, int value) {
  std::pair<unsigned int, unsigned int> minmax = param.map_paramid_minmax[bar];
  if (minmax.second>0) {
    value = minmax.first + value * (minmax.second - minmax.first) / 100;
    videodispatch.SetProperty(bar, value);
  }
  param.print_parameter(bar);
}

void on_brightness_trackbar( int value, void* ) {
  on_common_trackbar("Brightness", value);
}

void on_expose_trackbar(int value, void*) {
  on_common_trackbar("Expose", value);
}

void on_gain_trackbar(int value, void*) {
  on_common_trackbar("Gain", value);
}

void on_shutter_trackbar(int value, void*) {
  on_common_trackbar("Shutter", value);
}

typedef void TrackbarCallback(int, void*);

// sample program for  Rotation tracking
int main(int argc, char * argv[]) {

  videodispatch.SetProperty("Brightness", 50);
  videodispatch.SetProperty("Exposure", 40);
  videodispatch.SetProperty("Gain", 40);

  param = videodispatch.GetParameter();

  cv::namedWindow(WINDOW_NAME, CV_WINDOW_AUTOSIZE);

  std::array<std::string, 4> bars = { "Brightness", "Expose", "Gain", "Shutter" };
  std::array<TrackbarCallback*, 4> cbs = { &on_brightness_trackbar,
  &on_expose_trackbar, &on_gain_trackbar, &on_shutter_trackbar};

  for (int i = 0; i < bars.size(); ++i) {
    cv::createTrackbar(bars[i], WINDOW_NAME, 0, 100, *cbs[i]);
  }

  sense::SenseSubscriber video_rec;
  video_rec.Register(videodispatch);

  cv::Mat cur_frame, m_frame_bgr;
  bool stopped = false;
  char key_code;

  while (!stopped) {
    while (!video_rec.empty()) {
      cur_frame = std::dynamic_pointer_cast<ImageEvent>(video_rec.pop())->frame();
      cv::cvtColor(cur_frame, m_frame_bgr, CV_RGB2BGR); //! For weird OpenCV representation
      cv::imshow(WINDOW_NAME, m_frame_bgr);
    }

    //  Key press handling
    key_code = cv::waitKey(5);
    if (key_code == 27) {
      break;
    } 
  }  // end loop
  videodispatch.Stop();
  return 1;
}
