#include <opencv2/highgui/highgui.hpp>

#include "sense/sensor_recorder.h"
#include "sense/openni2_capture.h"
#include "sense/util.h"

// Draw cross in the middle of image
inline void DrawCross(cv::Mat& bgr_img,
                      const cv::Scalar& color = cv::Scalar(0, 0, 255)) {
  cv::Point mid_point(bgr_img.cols/2, bgr_img.rows/2);
  cv::line(bgr_img,
           mid_point + cv::Point(-10, 0),
           mid_point + cv::Point(10, 0),
           color, 2);
  cv::line(bgr_img,
           mid_point + cv::Point(0, -10),
           mid_point + cv::Point(0, 10),
           color, 2);
}

int main(int argc, char** argv) {
  const std::string output_path = (argc > 1 ) ? argv[1] : ".";

  sense::OpenNI2Capture onicap(true, false);

  std::string rec_file = output_path + "/kinect_recorder.txt";
  printf("Save recording data to (%s)\n", rec_file.c_str());
  sense::SensorRecorder recorder(rec_file);
  //recorder.Register(onicap);

  sense::SenseSubscriber rec;
  rec.Register(onicap);

  cv::Mat cur_frame, cur_depth;
  bool stop = false;
  bool pausing = false;
  sense::ColorAndDepthEventPtr cur_evt;
  while (!stop) {
    while (!rec.empty() && !pausing) {
      cur_evt = std::dynamic_pointer_cast<sense::ColorAndDepthEvent>(rec.pop());
    }
    if (cur_evt) {
      cv::cvtColor(cur_evt->frame(), cur_frame, CV_RGB2BGR);
      cur_depth = cur_evt->depth_frame().clone();
      recorder.OnEventCallback(cur_evt);
    }

    if (!cur_frame.empty() && !cur_depth.empty()) {
      DrawCross(cur_frame);

      static cv::Mat hist_img;
      sense::GenerateHistogramImage(cur_depth, hist_img);

      static cv::Mat depth_color_img;
      sense::CombineMatrixes(hist_img, cur_frame, depth_color_img);
      cv::imshow("Depth-Color frame", depth_color_img);

    }

    //std::this_thread::sleep_for(std::chrono::milliseconds(25));

    //  Key press handling
    char key = cv::waitKey(30);
    switch (key) {
    case 27:
    case 'e':
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      if (pausing) printf("PAUSED\n");
      break;
    case 'r':
      recorder.toggle_recording();
      break;
    }
  }

  // calculate depth sensors parameters
  double width = cur_frame.cols;
  double height = cur_frame.rows;
  double fx = width/2.0/std::tan(onicap.GetColorHorizontalFieldOfView()/2);
  double fy = height / 2.0 / std::tan(onicap.GetColorVerticalFieldOfView() / 2);
  double cx = width/2.0 - 0.5;
  double cy = height/2.0 - 0.5;
  double k1 = 0, k2 = 0, k3 = 0;

  std::string fn = output_path + "/camera.txt";
  printf("Save color camera parameters to (%s)\n", fn.c_str());
  std::ofstream ofs(fn);
  ofs << "# depth sensors parameters\n";
  ofs << "# color camera intrinsics(width, height, fx, fy, cx, fy, k1, k2, k3)\n";
  ofs << "COLOR-CAM " << width << " " << height << " " << fx << " " << fy << " " << cx << " " << cy << " " << k1 << " " << k2 << " " << k3 << "\n";
}