#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "sense/sensor_replay.h"
#include "sense/tracking_events.h"
#include "sense/util.h"

using namespace std;
using namespace sense;

// sample program for sensor replay
int main(int argc, char * argv[] ) {
  std::string filename = (argc > 1) ? argv[1] : "./sensor_recorder.txt";
  printf("Replaying recorded data from %s\n", filename.c_str());
  SensorReplay replay(filename);
  SenseSubscriber rec;
  rec.Register(replay);
  cv::Mat cur_frame;
  bool stop = false, pausing = false;
  while (!stop) {
    //  Key press handling
    char key = cv::waitKey(20);
    switch (key) {
    case 27:
    case 'e':
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      if (pausing) printf("PAUSED\n");
      break;
    case 'r':
      break;
    }
    
    if (rec.empty()) continue;
    EventPtr temp_event = rec.pop();

    if (pausing) continue;

    if (std::dynamic_pointer_cast<ColorAndDepthEvent>(temp_event)) {
      auto evt = std::dynamic_pointer_cast<ColorAndDepthEvent>(temp_event);
      
        cv::cvtColor(evt->frame(), cur_frame, CV_RGB2BGR);

        static cv::Mat hist_img;
        GenerateHistogramImage(evt->depth_frame(), hist_img);

        static cv::Mat depth_color_img;
        CombineMatrixes(hist_img, cur_frame, depth_color_img);
        cv::imshow("Depth-Color frame", depth_color_img);
    } else if (std::dynamic_pointer_cast<ImageEvent>(temp_event)) {
      auto img_evt = std::dynamic_pointer_cast<ImageEvent>(temp_event);
      static cv::Mat cur_frame;
      cv::cvtColor(img_evt->frame(), cur_frame, CV_RGB2BGR);
      cv::imshow("Recordered data", cur_frame);
    } else if (std::dynamic_pointer_cast<InertialEvent>(temp_event)) {
      auto evt = std::dynamic_pointer_cast<InertialEvent>(temp_event);

      cout << "accel:" << evt->accel() << "\t"
           << "gyro:" << evt->gyro() << "\t"
           << "magnet:" << evt->magnet() << endl;
    } else if (std::dynamic_pointer_cast<LaserEvent>(temp_event)) {
      auto evt = std::dynamic_pointer_cast<LaserEvent>(temp_event);
      cout << "distance:" << evt->distance() << "\t"
           << "time:"  << evt->timestamp() << endl;
    }
  }
}