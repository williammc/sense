#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>

#include "xsens/xsensor.h"
#include "sense/sensor_recorder.h"
#include "sense/videocapture_dispatch.h"
#include "sense/pd5x_laserdriver.h"
#include "sense/xsens_capture.h"

using namespace std;
using namespace sense;

#if WIN32 
const char* laser_serialport = "//./COM4";
static std::string xsens_device = "//./COM3";
#else
const char* laser_serialport = "/dev/tty.usbserial-01U41QN4"
static std::string xsens_device = "/dev/ttyUSB0";
#endif

// sample program for sensor  recorder
int main(int argc, char * argv[] ) {
  const int camw = (argc == 3 ) ? std::atoi(argv[1]) : 640; 
  const int camh = (argc == 3 ) ? std::atoi(argv[2]) : 480;

  VideoCaptureDispatch videodispatch(0, CameraFamily::PGR_FIREFLY_MV);
  videodispatch.SetProperty("Brightness", 50);
  videodispatch.SetProperty("Exposure", 40);
  videodispatch.SetProperty("Gain", 40);

  //PD5xLaserdriver laserdriver(laser_serialport);
  //XSENSCapture iner(xsens_device);

  SensorRecorder recorder("./sensor_recorder.txt");
  recorder.Register(videodispatch);
  //recorder.Register(laserdriver);
  //recorder.Register(iner);

  cv::Mat cur_frame;
  bool stop = false;
  bool pausing = false;

  while (!stop) {
    if (!recorder.empty() && !pausing) {
      auto evt = std::dynamic_pointer_cast<ImageEvent>(recorder.pop());
      if (evt) {
        cv::cvtColor(evt->frame(), cur_frame, CV_RGB2BGR);
      }
    }

    if (!cur_frame.empty())
      cv::imshow("Sensor Recorder", cur_frame);

    //  Key press handling
    char key = cv::waitKey(20);
    switch (key) {
    case 27:
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      break;
    case 'r':
      recorder.toggle_recording();
      break;
    }
  }
  return 0;
}


