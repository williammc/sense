#include <iostream>
#include <look3d/util/opencv.h>
#include "sensor/depth_helpers.h"
#include "sensor/extended_sensor_replay.h"

#include "sensor/config.h"

using namespace std;
using namespace look3d;

// sample program for sensor replay
int main(int argc, char * argv[] ) {
  string configfile = (argc > 1) ? argv[1] : CONFIG_FILE;
  cout << " loading config  file from: " << configfile << endl;

  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(configfile, pt);

  ExtendedSensorReplay replay(pt.get<string>("sensor.replay_filename"), true, 0);

  while (!replay.stop() && cv::waitKey(10) != 27) {
    if (replay.empty()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      continue;
    }
    std::shared_ptr<Event> temp_event = replay.pop();
    ColorAndDepthEvent * color_depth_event = dynamic_cast<ColorAndDepthEvent *>(temp_event.get());
    if (color_depth_event) {
      static cv::Mat bgr_img;
      cv::cvtColor(color_depth_event->frame, bgr_img, CV_RGB2BGR);
      static cv::Mat hist_img;
      generate_histogram_image(color_depth_event->depth_frame, hist_img);

      static cv::Mat depth_color_img;
      CombineMatrixes(hist_img, bgr_img, depth_color_img);
      cv::imshow("Depth-Color frame", depth_color_img);
      continue;
    }
  }
  return 1;
}




