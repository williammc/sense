#pragma once
#include <array>
#include <chrono>
#include <map>
#include <string>

// xsens
#include "internal/cmt3.h"
#include "internal/xsens_list.h"
#include "internal/cmtscan.h"

namespace xsens {

static double GetCurrentTime() {
  return std::chrono::high_resolution_clock::to_time_t(std::chrono::high_resolution_clock::now());
}

class XSensor {
 public:

  XSensor();
  XSensor(const std::string & device,
             CmtOutputMode output_mode_ = CMT_OUTPUTMODE_CALIB,
             CmtOutputSettings output_settings_ = CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION);

  ~XSensor();

  bool Open(const std::string & device,
            CmtOutputMode output_mode_ = CMT_OUTPUTMODE_CALIB,
            CmtOutputSettings output_settings_ = CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION);
  bool Config(CmtOutputMode output_mode_, CmtOutputSettings output_settings_);

  bool GetMeasurement(int & counter, std::array<float, 3> & acc,
                      std::array<float, 3> & gyro, std::array<float, 3> & mag,
                      double & timestamp);

  bool GetMeasurement(int & counter, std::array<float, 3> & acc,
                      std::array<float, 3> & gyro, std::array<float, 3> & mag,
                      std::array<float, 3>& euler, std::array<float, 4>& quad,
                      std::array<float, 9>& matrix,
                      double & timestamp);

  void Close(void);

  inline bool IsOpen(void) {
    return cmt3_.isPortOpen();
  }

  double CalibrateStartTime(int samples = 10);

  int mt_count() const { return mt_count_; }
  xsens::Packet* packet() const { return packet_; }
  xsens::Cmt3* cmt3() { return &cmt3_; }

 protected:
  unsigned int mt_count_;
  xsens::Packet* packet_;
  // supported mode
  // CMT_OUTPUTMODE_CALIB;
  // CMT_OUTPUTMODE_ORIENT;
  // CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT;
  // CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_CALIB;
  // CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_ORIENT;
  // CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT;
  CmtOutputMode output_mode_;

  // supported settings
  // CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION;
  // CMT_OUTPUTSETTINGS_ORIENTMODE_EULER;
  // CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX;
  CmtOutputSettings output_settings_;

  CmtDeviceId device_ids_[256];
  xsens::Cmt3 cmt3_;
};
}  // namespace