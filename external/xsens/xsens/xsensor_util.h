#pragma once

#ifdef WIN32
#include <conio.h>  // included for _getch and _kbhit
#else
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

namespace {

inline int kbhit(void) {
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if (ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}
}  // namespace
#endif

namespace {

// Sets the cursor position at the specified console position
//
// Input
//	 x	: New horizontal cursor position
//   y	: New vertical cursor position
inline void gotoxy(int x, int y) {
#ifdef WIN32
  COORD coord;
  coord.X = x;
  coord.Y = y;
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
#else
  char essq[100];		// String variable to hold the escape sequence
  char xstr[100];		// Strings to hold the x and y coordinates
  char ystr[100];		// Escape sequences must be built with characters

  /*
  ** Convert the screen coordinates to strings
  */
  sprintf(xstr, "%d", x);
  sprintf(ystr, "%d", y);

  /*
  ** Build the escape sequence (vertical move)
  */
  essq[0] = '\0';
  strcat(essq, "\033[");
  strcat(essq, ystr);

  /*
  ** Described in man terminfo as vpa=\E[%p1%dd
  ** Vertical position absolute
  */
  strcat(essq, "d");

  /*
  ** Horizontal move
  ** Horizontal position absolute
  */
  strcat(essq, "\033[");
  strcat(essq, xstr);
  // Described in man terminfo as hpa=\E[%p1%dG
  strcat(essq, "G");

  /*
  ** Execute the escape sequence
  ** This will move the cursor to x, y
  */
  printf("%s", essq);
#endif
}

// Clear console screen
inline void clrscr() {
#ifdef WIN32
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
  COORD coord = {0, 0};
  DWORD count;

  GetConsoleScreenBufferInfo(hStdOut, &csbi);
  FillConsoleOutputCharacter(hStdOut, ' ', csbi.dwSize.X * csbi.dwSize.Y, coord, &count);
  SetConsoleCursorPosition(hStdOut, coord);
#else
  int i;

  for (i = 0; i < 100; i++)
    // Insert new lines to create a blank screen
    putchar('\n');
  gotoxy(0,0);
#endif
}

//template<typename Derived>
//std::istream & operator >> (std::istream& in, Eigen::DenseBase<Derived>& m){
//  for(unsigned r=0; r<m.rows(); ++r)
//    for(unsigned c=0; c<m.cols(); ++c)
//      in >> m(r,c);
//  return in;
//}
}  // namespace xsens