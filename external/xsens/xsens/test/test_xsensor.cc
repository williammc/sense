#include <iostream>
#include <fstream>
#include <string>

using namespace std;
//using namespace xsens;

#include <xsensor.h>
#include <xsensor_util.h>

using xsens::XSensor;

int main(int argc, char *argv[]){
  string device = "/dev/ttyUSB0";
  ofstream file;

  XSensor xsens;

  int counter;
  Eigen::Vector3d acc, mag, gyr;
  double t, firstTime;

  double before = time(NULL);
  if (!xsens.open(device)) {
    cout << "Cannot open COM port " <<  device  << endl;
    return 1;
  }
  double after =  time(NULL);
  xsens.getMeasurement(counter, acc, gyr, mag, firstTime);
  cout << counter << " " << firstTime << " " << firstTime - counter * 0.01 << endl;
  clrscr();

  bool stop = false;
  while (!stop) {

#ifdef WIN32
    if (_kbhit())
#else
    if(kbhit())
#endif
      stop = true;

    xsens.getMeasurement(counter, acc, gyr, mag, t);

    gotoxy(0, 1);
    printf("acc: %6.3f\t%6.3f\t%6.3f" , acc[0], acc[1], acc[2]);

    gotoxy(0, 2);
    printf("gyr: %6.3f\t%6.3f\t%6.3f" , gyr[0], gyr[1], gyr[2]);

    gotoxy(0, 3);
    printf("mag: %6.3f\t%6.3f\t%6.3f" , mag[0], mag[1], mag[2]);
  }

  xsens.close();
  return 0;
}

