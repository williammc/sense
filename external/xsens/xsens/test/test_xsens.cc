#include <stdio.h>		// Needed for printf etc
#ifdef WIN32
#include <objbase.h>	// Needed for COM functionality
#include <conio.h>		// included for _getch and _kbhit
#endif
#include <internal/cmt3.h>
#include <internal/xsens_list.h>
#include <internal/cmtscan.h>
#include <xsensor_util.h>

// this macro tests for an error and exits the program with a message if there was one
#define EXIT_ON_ERROR(res,comment) if (res != XRV_OK) { printf("Error %d occurred in " comment ": %s\n",res,xsensResultText(res)); exit(1); }

void doHardwareScan();
void doMtSettings(void);
void getUserInputs(void);
void calcScreenOffset(void);
void writeHeaders(void);
void exitFunc(void);

using namespace xsens;

// used to signal that the user initiated the exit, so we do not wait for an extra keypress-
int userQuit = 0;
CmtOutputMode mode;
CmtOutputSettings settings;
unsigned long mtCount = 0;
int screenSensorOffset = 0;
int temperatureOffset = 0;
CmtDeviceId deviceIds[256];
xsens::Cmt3 cmt3;

int main(void) {
  XsensResultValue res = XRV_OK;
  short screenSkipFactor = 10;
  short screenSkipFactorCnt = screenSkipFactor;

  // Set exit function
  atexit(exitFunc);

  // Perform hardware scan
  doHardwareScan();

  // Give user a (short) chance to see hardware scan results
  //Sleep(2000);

  //clear screen present & get the user output mode selection.
  clrscr();
  getUserInputs();

  // Set device to user input settings
  doMtSettings();

  // Wait for first data item(s) to arrive. In production code, you would use a callback function instead (see cmtRegisterCallback function)
//  Sleep(20);

  //get the placement offsets, clear the screen and write the fixed headers.
  calcScreenOffset();
  clrscr();
  writeHeaders();

  // vars for sample counter & temp.
  unsigned short sdata;
  double tdata;

  //structs to hold data.
  CmtCalData caldata;
  CmtQuat qat_data;
  CmtEuler euler_data;
  CmtMatrix matrix_data;

  // Initialize packet for data
  Packet* packet = new Packet((unsigned short)mtCount,cmt3.isXm());

  while (!userQuit && res == XRV_OK) {
    cmt3.waitForDataMessage(packet);

    //get sample count, goto position & display.
    sdata = packet->getSampleCounter();
    gotoxy(0,0);
    printf("Sample Counter %05hu\n", sdata);

    if (screenSkipFactorCnt++ == screenSkipFactor) {
      screenSkipFactorCnt = 0;

      for (unsigned int i = 0; i < mtCount; i++) {
        // Output Temperature
        if ((mode & CMT_OUTPUTMODE_TEMP) != 0) {
          gotoxy(0,4 + i * screenSensorOffset);
          tdata = packet->getTemp(i);
          printf("%6.2f", tdata);
        }

        gotoxy(0,5 + temperatureOffset + i * screenSensorOffset);	// Output Calibrated data
        if ((mode & CMT_OUTPUTMODE_CALIB) != 0) {
          caldata = packet->getCalData(i);
          printf("%6.2f\t%6.2f\t%6.2f" , caldata.m_acc.m_data[0], caldata.m_acc.m_data[1], caldata.m_acc.m_data[2]);

          gotoxy(0,7 + temperatureOffset + i * screenSensorOffset);
          printf("%6.2f\t%6.2f\t%6.2f", caldata.m_gyr.m_data[0], caldata.m_gyr.m_data[1], caldata.m_gyr.m_data[2] );

          gotoxy(0,9 + temperatureOffset + i * screenSensorOffset);
          printf("%6.2f\t%6.2f\t%6.2f",caldata.m_mag.m_data[0], caldata.m_mag.m_data[1], caldata.m_mag.m_data[2]);
          gotoxy(0,13 + temperatureOffset + i * screenSensorOffset);
        }

        if ((mode & CMT_OUTPUTMODE_ORIENT) != 0) {
          switch(settings & CMT_OUTPUTSETTINGS_ORIENTMODE_MASK) {
          case CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION:
            // Output: quaternion
            qat_data = packet->getOriQuat(i);
            printf("%6.3f\t%6.3f\t%6.3f\t%6.3f\n", qat_data.m_data[0], qat_data.m_data[1],qat_data.m_data[2],qat_data.m_data[3]);
            break;

          case CMT_OUTPUTSETTINGS_ORIENTMODE_EULER:
            // Output: Euler
            euler_data = packet->getOriEuler(i);
            printf("%6.1f\t%6.1f\t%6.1f\n", euler_data.m_roll,euler_data.m_pitch, euler_data.m_yaw);
            break;

          case CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX:
            // Output: Cosine Matrix
            matrix_data = packet->getOriMatrix(i);
            printf("%6.3f\t%6.3f\t%6.3f\n", matrix_data.m_data[0][0],matrix_data.m_data[0][1], matrix_data.m_data[0][2]);
            printf("%6.3f\t%6.3f\t%6.3f\n", matrix_data.m_data[1][0],matrix_data.m_data[1][1], matrix_data.m_data[1][2]);
            printf("%6.3f\t%6.3f\t%6.3f\n", matrix_data.m_data[2][0],matrix_data.m_data[2][1], matrix_data.m_data[2][2]);
            break;
          default:
            ;
          }
        }
      }
    }
#ifdef WIN32
    if (_kbhit())
#else
    if(kbhit())
#endif
      userQuit = 1;
  }

  delete packet;

  clrscr();
  cmt3.closePort();

  return 0;
}

//////////////////////////////////////////////////////////////////////////
// doHardwareScan
//
// Checks available COM ports and scans for MotionTrackers
void doHardwareScan() {
  XsensResultValue res;
  List<CmtPortInfo> portInfo;
  unsigned long portCount = 0;

  printf("Scanning for connected Xsens devices...");
  xsens::cmtScanPorts(portInfo, 115200);
  portCount = portInfo.length();
  printf("done\n");

  if (portCount == 0) {
    printf("No Xsens devices found\n\n");
    exit(0);
  }

  for(int i = 0; i < (int)portCount; i++) {
    printf("Using COM port %d at %d baud\n\n",
      portInfo[i].m_portNr, portInfo[i].m_baudrate);
  }

  printf("Opening ports...");
  //open the port which the device is connected to and connect at the device's baudrate.
  for(int p = 0; p < (int)portCount; p++){
    char port_name[200];
#ifdef WIN32
    sprintf(port_name, "COM%d", portInfo[p].m_portNr);
#else
    sprintf(port_name, "%s", portInfo[p].m_portName);
#endif
    res = cmt3.openPort(port_name, portInfo[p].m_baudrate);
    EXIT_ON_ERROR(res,"cmtOpenPort");
  }
  printf("done\n\n");

  //get the Mt sensor count.
  printf("Retrieving MotionTracker count (excluding attached Xbus Master(s))\n");
  mtCount = cmt3.getMtCount();
  mtCount = mtCount;
  printf("MotionTracker count: %d\n\n",mtCount);

  // retrieve the device IDs
  printf("Retrieving MotionTrackers device ID(s)\n");
  for(unsigned int j = 0; j < mtCount; j++ ){
    res = cmt3.getDeviceId((unsigned char)(j+1), deviceIds[j]);
    EXIT_ON_ERROR(res,"getDeviceId");
    printf("Device ID at busId %d: %08x\n",j+1,(long) deviceIds[j]);
  }
}

//////////////////////////////////////////////////////////////////////////
// getUserInputs
//
// Request user for output data
void getUserInputs() {
  do {
    printf("Select desired output:\n");
    printf("1 - Calibrated data\n");
    printf("2 - Orientation data\n");
    printf("3 - Both Calibrated and Orientation data\n");
    printf("4 - Temperature and Calibrated data\n");
    printf("5 - Temperature and Orientation data\n");
    printf("6 - Temperature, Calibrated and Orientation data\n");
    printf("Enter your choice: ");
    scanf("%d", &mode);
    // flush stdin
    while (getchar() != '\n') continue;

    if (mode < 1 || mode > 6) {
      printf("\n\nPlease enter a valid output mode\n");
    }
  } while(mode < 1 || mode > 6);
  clrscr();

  switch(mode) {
  case 1:
    mode = CMT_OUTPUTMODE_CALIB;
    break;
  case 2:
    mode = CMT_OUTPUTMODE_ORIENT;
    break;
  case 3:
    mode = CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT;
    break;
  case 4:
    mode = CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_CALIB;
    break;
  case 5:
    mode = CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_ORIENT;
    break;
  case 6:
    mode = CMT_OUTPUTMODE_TEMP | CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT;
    break;
  }

  if ((mode & CMT_OUTPUTMODE_ORIENT) != 0) {
    do {
      printf("Select desired output format\n");
      printf("1 - Quaternions\n");
      printf("2 - Euler angles\n");
      printf("3 - Matrix\n");
      printf("Enter your choice: ");
      scanf("%d", &settings);
      // flush stdin
      while (getchar() != '\n') continue;

      if (settings < 1  || settings > 3) {
        printf("\n\nPlease enter a valid choice\n");
      }
    } while(settings < 1 || settings > 3);

    // Update outputSettings to match data specs of SetOutputSettings
    switch (settings) {
    case 1:
      settings = CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION;
      break;
    case 2:
      settings = CMT_OUTPUTSETTINGS_ORIENTMODE_EULER;
      break;
    case 3:
      settings = CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX;
      break;
    }
  } else {
    settings = 0;
  }
  settings |= CMT_OUTPUTSETTINGS_TIMESTAMP_SAMPLECNT;
}

//////////////////////////////////////////////////////////////////////////
// doMTSettings
//
// Set user settings in MTi/MTx
// Assumes initialized cmt3 class with open COM port
void doMtSettings(void) {
  XsensResultValue res;

  // set sensor to config sate
  res = cmt3.gotoConfig();
  EXIT_ON_ERROR(res,"gotoConfig");

  unsigned short sampleFreq;
  sampleFreq = cmt3.getSampleFrequency();

  // set the device output mode for the device(s)
  printf("Configuring your mode selection");
  CmtDeviceMode deviceMode(mode, settings, sampleFreq);
  for(unsigned int i = 0; i < mtCount; i++){
    res = cmt3.setDeviceMode(deviceMode,true, deviceIds[i]);
    EXIT_ON_ERROR(res,"setDeviceMode");
  }

  // start receiving data
  res = cmt3.gotoMeasurement();
  EXIT_ON_ERROR(res,"gotoMeasurement");
}

//////////////////////////////////////////////////////////////////////////
// writeHeaders
//
// Write appropriate headers to screen
void writeHeaders() {
  for (unsigned int i = 0; i < mtCount; i++) {
    gotoxy(0, 2 + i * screenSensorOffset);
    printf("MotionTracker %d\n", i + 1);

    if ((mode & CMT_OUTPUTMODE_TEMP) != 0) {
      temperatureOffset = 3;
      gotoxy(0,3 + i * screenSensorOffset);
      printf("Temperature");
      gotoxy(7,4 + i * screenSensorOffset);
      printf("degrees celcius");
      gotoxy(0,6 + i * screenSensorOffset);
    }

    if ((mode & CMT_OUTPUTMODE_CALIB) != 0) {
      gotoxy(0,3 + temperatureOffset + i * screenSensorOffset);
      printf("Calibrated sensor data");
      gotoxy(0,4 + temperatureOffset + i * screenSensorOffset);
      printf(" Acc X\t Acc Y\t Acc Z");
      gotoxy(23, 5 + temperatureOffset + i * screenSensorOffset);
      printf("(m/s^2)");
      gotoxy(0,6 + temperatureOffset + i * screenSensorOffset);
      printf(" Gyr X\t Gyr Y\t Gyr Z");
      gotoxy(23, 7 + temperatureOffset + i * screenSensorOffset);
      printf("(rad/s)");
      gotoxy(0,8 + temperatureOffset + i * screenSensorOffset);
      printf(" Mag X\t Mag Y\t Mag Z");
      gotoxy(23, 9 + temperatureOffset + i * screenSensorOffset);
      printf("(a.u.)");
      gotoxy(0,11 + temperatureOffset + i * screenSensorOffset);
    }

    if ((mode & CMT_OUTPUTMODE_ORIENT) != 0) {
      printf("Orientation data\n");
      switch(settings & CMT_OUTPUTSETTINGS_ORIENTMODE_MASK) {
      case CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION:
        printf("    q0\t    q1\t    q2\t    q3\n");
        break;
      case CMT_OUTPUTSETTINGS_ORIENTMODE_EULER:
        printf("  Roll\t Pitch\t   Yaw\n");
        printf("                       degrees\n");
        break;
      case CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX:
        printf(" Matrix\n");
        break;
      default:
        ;
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////
// calcScreenOffset
//
// Calculates offset for screen data with multiple sensors.
void calcScreenOffset() {
  // 1 line for "Sensor ..."
  screenSensorOffset += 1;
  if ((mode & CMT_OUTPUTMODE_TEMP) != 0)
    screenSensorOffset += 3;
  if ((mode & CMT_OUTPUTMODE_CALIB) != 0)
    screenSensorOffset += 8;
  if ((mode & CMT_OUTPUTMODE_ORIENT) != 0) {
    switch(settings & CMT_OUTPUTSETTINGS_ORIENTMODE_MASK) {
    case CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION:
      screenSensorOffset += 4;
      break;
    case CMT_OUTPUTSETTINGS_ORIENTMODE_EULER:
      screenSensorOffset += 4;
      break;
    case CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX:
      screenSensorOffset += 6;
      break;
    default:
      ;
    }
  }
}

//////////////////////////////////////////////////////////////////////////
// exitFunc
//
// Closes cmt nicely
void exitFunc(void) {
  // Close any open COM ports
  cmt3.closePort();

  // get rid of keystrokes before we post our message
//  while (_kbhit()) _getch();

  // wait for a keypress
  if (!userQuit) {
    printf("Press a key to exit\n");
//    _getch();
  }
}

