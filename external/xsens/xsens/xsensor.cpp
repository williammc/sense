#include "xsensor.h"

#include <iostream>
#include <vector>

using namespace std;

namespace xsens {

  XSensor::XSensor()
    : mt_count_(0), packet_(NULL) {}

  XSensor::XSensor(const string &device,
                   CmtOutputMode output_mode,
                   CmtOutputSettings output_settings)
                   : mt_count_(0), packet_(NULL) {
    bool ret = Open(device, output_mode, output_settings);
    if (!ret)
      throw "could not open device";
  }

  XSensor::~XSensor() {
    Close();
  }

  bool XSensor::Open(const string &device,
                     CmtOutputMode output_mode,
                     CmtOutputSettings output_settings) {
    XsensResultValue res;
    List<CmtPortInfo> ports_info;

    printf("Scanning for connected Xsens devices...");
    xsens::cmtScanPorts(ports_info, 115200);
    unsigned long port_count = ports_info.length();
    printf("done\n");

    if (port_count == 0) {
      printf("No Xsens devices found\n\n");
      return false;
    }

    for (int i = 0; i < (int)port_count; i++) {
      printf("Using port %d at %d baud\n\n",
             (long)ports_info[i].m_portNr, ports_info[i].m_baudrate);
    }

    printf("Opening ports...");
    //open the port which the device is connected to and connect at the device's baudrate.
    for (int p = 0; p < (int)port_count; p++) {
      char port_name[200];
#ifdef WIN32
      sprintf(port_name, "COM%d", ports_info[p].m_portNr);
#else
      sprintf(port_name, "%s", ports_info[p].m_portName);
#endif
      res = cmt3_.openPort(port_name, ports_info[p].m_baudrate);
      if (res != XRV_OK) {
        printf("Cannot open port with given baudrate\n");
        return false;
      }
    }
    printf("done\n\n");

    //get the Mt sensor count.
    printf("Retrieving MotionTracker count (excluding attached Xbus Master(s))\n");
    mt_count_ = cmt3_.getMtCount();
    mt_count_ = mt_count_;
    printf("MotionTracker count: %i\n\n", mt_count_);

    // retrieve the device IDs
    printf("Retrieving MotionTrackers device ID(s)\n");
    for (unsigned int j = 0; j < mt_count_; j++) {
      res = cmt3_.getDeviceId((unsigned char)(j + 1), device_ids_[j]);
      if (res != XRV_OK) {
        printf("Cannot getDeviceId\n");
        return false;
      }
      printf("Device ID at busId %i: %08x\n", j + 1, (long)device_ids_[j]);
    }
    return Config(output_mode, output_settings);
  }

  bool XSensor::Config(CmtOutputMode output_mode,
                       CmtOutputSettings output_settings) {
    // set sensor to config sate
    XsensResultValue res = cmt3_.gotoConfig();
    if (res != XRV_OK)
      return false;

    unsigned short sample_freq;
    sample_freq = cmt3_.getSampleFrequency();

    // set the device output mode for the device(s)
    printf("Configuring your mode selection");
    output_settings |= CMT_OUTPUTSETTINGS_TIMESTAMP_SAMPLECNT;
    CmtDeviceMode deviceMode(output_mode, output_settings, sample_freq);
    for (unsigned int i = 0; i < mt_count_; i++) {
      res = cmt3_.setDeviceMode(deviceMode, true, device_ids_[i]);
      if (res != XRV_OK) {
        printf("Cannot setDeviceMode\n");
        return false;
      }
    }

    // start receiving data
    res = cmt3_.gotoMeasurement();
    if (res != XRV_OK) {
      printf("Cannot gotoMeasurement\n");
      return false;
    }

    this->output_mode_ = output_mode;
    this->output_settings_ = output_settings;
    return true;
  }

  bool XSensor::GetMeasurement(
    int & counter, std::array<float, 3> & acc,
    std::array<float, 3> & gyro, std::array<float, 3> & mag,
    double & timestamp) {

    timestamp = GetCurrentTime();

    //structs to hold data.
    CmtCalData caldata;

    // Initialize packet for data
    if (packet_ == NULL)
      packet_ = new Packet((unsigned short)mt_count_, cmt3_.isXm());
    cmt3_.waitForDataMessage(packet_);

    //get sample count
    unsigned short sdata = packet_->getSampleCounter();

    double tdata; // temperature data
    for (unsigned int i = 0; i < mt_count_; i++) {
      if ((output_mode_ & CMT_OUTPUTMODE_TEMP) != 0) {
        tdata = packet_->getTemp(i);
        printf("%6.2f", tdata);
      }
      if ((output_mode_ & CMT_OUTPUTMODE_CALIB) != 0) {
        caldata = packet_->getCalData(i);
        acc = std::array < float, 3 > { caldata.m_acc.m_data[0], caldata.m_acc.m_data[1], caldata.m_acc.m_data[2]};
        gyro = std::array < float, 3 > { caldata.m_gyr.m_data[0], caldata.m_gyr.m_data[1], caldata.m_gyr.m_data[2]};
        mag = std::array < float, 3 > { caldata.m_mag.m_data[0], caldata.m_mag.m_data[1], caldata.m_mag.m_data[2]};
      } else {
        return false;
      }
    }
    return true;
  }

  bool XSensor::GetMeasurement(
    int& counter, std::array<float, 3> & acc,
    std::array<float, 3> & gyro, std::array<float, 3> & mag,
    std::array<float, 3>& euler, std::array<float, 4>& quad,
    std::array<float, 9>& matrix,
    double & timestamp) {

    timestamp = GetCurrentTime();

    //structs to hold data.
    CmtCalData caldata;
    CmtQuat qat_data;
    CmtEuler euler_data;
    CmtMatrix matrix_data;

    // Initialize packet for data
    static bool first = true;
    if (first) {
      first = false;
      packet_ = new Packet((unsigned short)mt_count_, cmt3_.isXm());
    }
    cmt3_.waitForDataMessage(packet_);

    //get sample count
    unsigned short sdata = packet_->getSampleCounter();

    double tdata; // temperature data
    for (unsigned int i = 0; i < mt_count_; i++) {
      if ((output_mode_ & CMT_OUTPUTMODE_TEMP) != 0) {
        tdata = packet_->getTemp(i);
        printf("%6.2f", tdata);
      }
      if ((output_mode_ & CMT_OUTPUTMODE_CALIB) != 0) {
        caldata = packet_->getCalData(i);
        acc = std::array < float, 3 > { caldata.m_acc.m_data[0], caldata.m_acc.m_data[1], caldata.m_acc.m_data[2]};
        gyro = std::array < float, 3 > { caldata.m_gyr.m_data[0], caldata.m_gyr.m_data[1], caldata.m_gyr.m_data[2]};
        mag = std::array < float, 3 > { caldata.m_mag.m_data[0], caldata.m_mag.m_data[1], caldata.m_mag.m_data[2]};
      }
      if ((output_mode_ & CMT_OUTPUTMODE_ORIENT) != 0) {
        switch (output_settings_ & CMT_OUTPUTSETTINGS_ORIENTMODE_MASK) {
        case CMT_OUTPUTSETTINGS_ORIENTMODE_QUATERNION:
          // Output: quaternion
          qat_data = packet_->getOriQuat(i);
          quad = std::array < float, 4 > {qat_data.m_data[0], qat_data.m_data[1],
            qat_data.m_data[2], qat_data.m_data[3]};
          break;

        case CMT_OUTPUTSETTINGS_ORIENTMODE_EULER:
          // Output: Euler
          euler_data = packet_->getOriEuler(i);
          euler = std::array < float, 3 > {euler_data.m_roll, euler_data.m_pitch, euler_data.m_yaw};
          break;

        case CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX:
          // Output: Cosine Matrix
          matrix_data = packet_->getOriMatrix(i);
          matrix = std::array < float, 9 > { 
            matrix_data.m_data[0][0], matrix_data.m_data[0][1], matrix_data.m_data[0][2],
            matrix_data.m_data[1][0], matrix_data.m_data[1][1], matrix_data.m_data[1][2],
            matrix_data.m_data[2][0], matrix_data.m_data[2][1], matrix_data.m_data[2][2] };
          break;
        }
      }
    }
    return true;
  }

  void XSensor::Close(void) {
    if (cmt3_.isPortOpen()) {
      cmt3_.closePort();
      if (packet_)
        delete packet_;
    }
  }
}  // namespace xsens