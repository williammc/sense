Sense library and applications
Version 1.0.0

# About Sense 
`Sense` is a library for communicating with sensors (e.x.: camera, xsens, laser, etc).
It is written in C++ and is complied with C++11 standard.

## Version 1.0.0 (November 2014)

# Coding Guideline
`Sense` library and apps follows the C++ Style Guide from [googlecode.com](http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)
- tools: used to check format & raise useful tips (~/scripts/cpplint.py)
         (this tools is from the C++ Style Guide resource)
- code formating: check [clang-format](http://clang.llvm.org/docs/ClangFormat.html) tool.

# DEPENDENCIES
## Required dependencies for Sense library
- `Slick` (included in ~/root/external folder)
