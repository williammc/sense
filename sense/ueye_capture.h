// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <uEye.h>

#include "sense/generic_capture.h"

namespace sense {
struct UEyeProperty {
  UEyeProperty() {}
  UEyeProperty(UINT sc, UINT gc, UINT rmin = 0, UINT rmax = 0)
    : set_command(sc), get_command(gc), range_min_command(rmin), range_max_command(rmax)
  {}

  UINT set_command;
  UINT get_command;
  UINT range_min_command;
  UINT range_max_command;
};

class UEyeCapture : public GenericCapture {
 public:
  UEyeCapture() = delete;
  UEyeCapture(const UEyeCapture&) = delete;
  UEyeCapture(std::string const& param_file,
            int camid, 
            CameraFamily::ID camfam = CameraFamily::GENERIC,
            INT color_mode = IS_CM_RGB8_PACKED ,
            HWND display_mode = NULL);

  UEyeCapture(int camid, CameraFamily::ID camfam = CameraFamily::GENERIC,
            INT color_mode = IS_CM_RGB8_PACKED ,
            HWND display_mode = NULL,
            SenseScalar frame_rate = 30);
  ~UEyeCapture();

  bool Open(int camid, std::string const& param_file,
            CameraFamily::ID camfam = CameraFamily::GENERIC,
            INT color_mode = IS_CM_RGB8_PACKED ,
            HWND display_mode = NULL);
  bool Open(int camid, CameraFamily::ID camfam = CameraFamily::GENERIC,
            INT color_mode = IS_CM_RGB8_PACKED ,
            HWND display_mode = NULL,
            SenseScalar frame_rate = 30);

  void GetMaxImageSize(SENSORINFO &sensor_info, unsigned *width, unsigned *height);
  int InitDisplayMode(SENSORINFO const &si);
  
  // read latest frame
  EventPtr CaptureEvent() override;

  // proper shutdown for the camera
  void Release() override;

  HCAM& GetCamera();

 protected:
  unsigned frame_width_;
  unsigned frame_height_;
  CameraFamily::ID camera_family_;
  HCAM camera_;
  int color_mode_;
  int byte_perpixel_;
  int opencv_color_mode_;
  int display_mode_;
  SenseScalar frame_rate_;
  char* image_memory_;
  int memory_id_;
  std::map<std::string, UEyeProperty> map_string_property_;
};
}  // namespace sense