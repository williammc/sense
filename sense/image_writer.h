#pragma once
#include <atomic>
#include <memory>
#include <thread>
#include <opencv2/highgui/highgui.hpp>
#include "sense/sense_api.h"

namespace sense {
struct ImageWriter;

void SENSE_API WaitForWriterToEnd(ImageWriter **writer);

bool SENSE_API StoreImageCommon(ImageWriter **writer,
                                      const cv::Mat& frame,
                                      const std::string output_path,
                                      std::string& filename,
                                      const std::string &type,
                                      const std::string& prefix = "");

struct ImageWriter {
  ImageWriter() = delete;
  ImageWriter(const ImageWriter&) = delete;
  ImageWriter (const std::string & path, const cv::Size & size,
               const unsigned buffer_size = 1000);

  ~ImageWriter();

  void Run();
  void Stop();

  std::string writer_path;     ///< path to current writing folder
  int imagependding_counter;   ///< counter for currently inputing image
  int imagestored_counter;     ///< counter for stored image
  int start_index, end_index;
  std::vector<cv::Mat> image_buffer;
  std::vector<std::string> filename_buffer;
  std::atomic<bool> stopped;
  std::thread this_thread_;
};
}  // namespace sense