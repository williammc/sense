#pragma once
#include <string>
#include <OpenNI.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/sense_manager.h"

namespace sense {

  struct OpenNI2Capture : public SensePublisher {
    OpenNI2Capture(bool sync_capture = true, bool mirror_images = true);
    ~OpenNI2Capture();

    virtual EventPtr CaptureEvent();

    double GetColorHorizontalFieldOfView() const;
    double GetColorVerticalFieldOfView() const;
  protected:
    bool Init();

    // Capture color image & registered depth
    bool SyncCapture(double& timestamp, cv::Mat& rgb_img, cv::Mat& raw_depth_img);

    bool CaptureColor(double& timestamp, cv::Mat& rgb_img);

    bool CaptureDepth(double& timestamp, cv::Mat& depth_img);

    bool is_good_;
    bool sync_capture_;
    bool mirror_images_;
    bool need_manual_mirror_;
    openni::VideoFrameRef depth_frame_;
    openni::VideoFrameRef color_frame_;

    openni::Device device_;
    openni::VideoStream depth_stream_;
    openni::VideoStream color_stream_;
    openni::VideoStream** streams_;
  };
}  // namespace sense