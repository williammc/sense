// Copyright  2014 The Sense Authors. All rights reserved.
#pragma once
#include <fstream>
#include <memory>
#include "sense/generic_capture.h"

#if defined(SENSE_HAVE_DC1394_2)
#include "sense/dc1394_capture.h"
#endif
#if defined(SENSE_HAVE_FLYCAPTURE2)
#include "sense/flycapture.h"
#endif
#if defined(SENSE_HAVE_UEYE)
#include "sense/ueye_capture.h"
#endif

namespace sense {

class VideoCaptureDispatch : public SensePublisher {
protected:
  enum SourceType {IMAGE = 0, IMAGE_SEQUENCE, VIDEO, LIVE};
  std::shared_ptr<GenericCapture> genericcapture;
  SourceType sourcetype;  // indicate source video is an image
  int image_sequence_index;
  std::string video_source;
public:
  VideoCaptureDispatch() = delete;
  VideoCaptureDispatch(const VideoCaptureDispatch&) = delete;
  VideoCaptureDispatch(int device, 
                       CameraFamily::ID camfam = CameraFamily::GENERIC,
                       std::string config_file = "");
  VideoCaptureDispatch(std::string filename);
  ~VideoCaptureDispatch();

  void SetProperty(std::string const& param, unsigned int value);
  unsigned int GetProperty(std::string const& param);
  bool SetHDR(bool on);

  // Returns the next frame from the buffer.
  // This function blocks until a frame is ready.
  EventPtr CaptureEvent();
  GenericCapture* GetVideoCapture();
  GenericParameter& GetParameter();

protected:
  CameraFamily::ID camfam_;
};
}  // namespace sense