// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <array>
#include <opencv2/core/core.hpp>
#include "sense/event_queue.h"
#include "sense/util.h"

namespace sense {

struct ImageEvent : public Event {
public:
  ImageEvent() : Event(-1) {}
  ~ImageEvent() {}
  ImageEvent(SenseScalar t, const cv::Mat& f) : Event(t), frame_(f) {}
  cv::Mat frame() { return frame_; }
protected:
  cv::Mat frame_;
};
using ImageEventPtr = std::shared_ptr<ImageEvent>;

struct InertialEvent : public Event {
  InertialEvent() = default;
  InertialEvent(SenseScalar t, const std::array<float, 3> &g,
                const std::array<float, 3> &a,
                const std::array<float, 3> &m ,const std::array<float, 4> &q)
    : Event(t), gyro_(g), accel_(a), magnet_(m),quat_(q) {}
  InertialEvent(SenseScalar t, const std::array<float, 3> &g,
                const std::array<float, 3> &a,
                const std::array<float, 3> &m ,const std::array<float, 9> &matrix )
    : Event(t), gyro_(g), accel_(a), magnet_(m),matrix_(matrix) {}
  
  std::array<float, 3> gyro() const { return gyro_; }
  std::array<float, 3> accel() const { return accel_; }
  std::array<float, 3> magnet() const { return magnet_; }

  std::array<float, 4> quaternion() const { return quat_; }

  std::array<float, 9> matrix() const { return matrix_; }

protected:
  std::array<float, 3> gyro_, accel_, magnet_;  ///< gyroscope, accelerometer, magnetic
  std::array<float, 4> quat_;    ///< quaternion
  std::array<float, 9> matrix_;  ///< 3x3 row-major matrix
};
using InertialEventPtr = std::shared_ptr<InertialEvent>;

struct GPSEvent : public Event {
  GPSEvent() = default;
  GPSEvent(SenseScalar t, const std::array<float, 3> & p,
           const int ns, const SenseScalar dd, const int f)
    : Event(t), position_(p), num_sat_(ns), diffdelay_(dd), fix_(f) {}

  std::array<float, 3> position() const { return position_; }
  int num_sat() const { return num_sat_; }
  SenseScalar diffdelay() const { return diffdelay_; }
  int fix() const { return fix_; }

protected:
  std::array<float, 3> position_;
  int num_sat_;
  SenseScalar diffdelay_;
  int fix_;
};
using GPSEventPtr = std::shared_ptr<GPSEvent>;

struct LaserEvent : public Event {
  LaserEvent() {
    distance_ = 0.0;
    timestamp_ = sense::Timestamp();
  }
  LaserEvent(double t, float dist) : Event(t), distance_(dist) {}

  float distance() const { return distance_; }
protected:
  float  distance_;
};
using LaserEventPtr = std::shared_ptr<LaserEvent>;


using ColorImgEvent = ImageEvent;

// Depth image from depth sensors
struct DepthImageEvent : public ImageEvent {
  DepthImageEvent() = delete;
  DepthImageEvent(SenseScalar t, cv::Mat depth_image, bool aligned = true)
    : ImageEvent(t, depth_image), aligned_to_color_(aligned) {}

  bool aligned_to_color() const { return aligned_to_color_; }

protected:
  bool aligned_to_color_;  // indiate whether depth values is aligned to color image
};
using DepthImageEventPtr = std::shared_ptr<DepthImageEvent>;

struct ColorAndDepthEvent : public ImageEvent {
  ColorAndDepthEvent() = delete;
  ColorAndDepthEvent(SenseScalar& t,
                     cv::Mat& color_image,
                     cv::Mat& depth_image,
                     bool aligned = true)
    : ImageEvent(t, color_image), depth_frame_(depth_image), 
      aligned_to_color_(aligned) {}

  cv::Mat depth_frame() const { return depth_frame_; }
  bool aligned_to_color() const { return aligned_to_color_; }
protected:
  cv::Mat depth_frame_;
  bool aligned_to_color_;  // indiate whether depth values is aligned to color image
};
using ColorAndDepthEventPtr = std::shared_ptr<ColorAndDepthEvent>;

struct GravityEvent : public Event {
  GravityEvent() {}
  GravityEvent(SenseScalar& t, std::array<float, 3>& grav)
    : Event(t), gravity_(grav) {}

  std::array<float, 3> gravity() { return gravity_; }
protected:
  std::array<float, 3> gravity_;
};
using GravityEventPtr = std::shared_ptr<GravityEvent>;
}  // namespace sense

namespace std {
// useful output input ========================================================
inline std::ostream& operator << (std::ostream& os,
                                  const std::array<float, 3>& v) {
  for (int i = 0; i < 3; ++i)
    os << v[i] << " ";
  return os;
}

inline std::istream& operator >> (std::istream& os,
                                  std::array<float, 3>& v) {
  for (int i = 0; i < 3; ++i)
    os >> v[i];
  return os;
}

inline std::ostream& operator << (std::ostream& os,
                                  const std::array<float, 4>& v) {
  for (int i = 0; i < 3; ++i)
    os << v[i] << " ";
  return os;
}

inline std::istream& operator >> (std::istream& os,
                                  std::array<float, 4>& v) {
  for (int i = 0; i < 3; ++i)
    os >> v[i];
  return os;
}
} // namespace std
