// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/ueye_capture.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "sense/tracking_events.h"

using namespace std;
namespace sense {
static void PrintCameraInfo(CAMINFO &cam_info) {
  printf("\n*** CAMERA INFORMATION ***\n"
         "Serial number - %s\n"
         "Camera Vendor - %s\n"
         "Camera Version - %s\n"
         "Camera Date - %s\n"
         "Camera Select - %d\n"
         "Camera Type - %d\n"
         "Camera Reserved - %s\n",
         cam_info.SerNo, cam_info.ID, cam_info.Version, cam_info.Date,
         cam_info.Select, cam_info.Type, cam_info.Reserved);
}

static void PrintSensorInfo(SENSORINFO &sensor_info) {
  printf("\n*** SENSOR INFORMATION ***\n"
         "SensorID - %d\n"
         "SensorName - %s\n"
         "ColorMode - %d\n"
         "MaxWidth - %d\n"
         "MaxHeight - %d\n"
         "Master Gain - %d\n"
         "Red Gain - %d\n"
         "Green Gain - %d\n"
         "Blue Gain - %d\n"
         "Global Shutter - %d\n"
         "Pixel Size - %d\n"
         "Reserved - %s\n",
         sensor_info.SensorID, sensor_info.strSensorName,
         sensor_info.nColorMode, sensor_info.nMaxWidth, sensor_info.nMaxHeight,
         sensor_info.bMasterGain, sensor_info.bRGain, sensor_info.bGGain,
         sensor_info.bBGain, sensor_info.bGlobShutter, sensor_info.wPixelSize,
         sensor_info.Reserved);
}

#ifdef WIN32
std::wstring string2wstring(const std::string &s) {
  int len;
  int slength = (int)s.length() + 1;
  len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
  wchar_t *buf = new wchar_t[len];
  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
  std::wstring r(buf);
  delete[] buf;
  return r;
}
#endif

UEyeCapture::UEyeCapture(std::string const &param_file,
                         int camid,
                         CameraFamily::ID camfam,
                         INT color_mode,
                         HWND display_mode) {
  identity_ = "ueye_capture-id" + std::to_string(camid) + "-family" +
              std::to_string(camfam);
  if (param_file.empty())
    is_good_ = Open(camid, camfam, color_mode, display_mode);
  else
    is_good_ = Open(camid, param_file, camfam, color_mode, display_mode);
  if (!is_good_) {
    printf("Cannot open UEye camera\n");
    return;
  }
  Run();
}

UEyeCapture::UEyeCapture(int camid, CameraFamily::ID camfam, INT color_mode,
                         HWND display_mode, SenseScalar frame_rate) {
  identity_ = "ueye_capture-id" + std::to_string(camid) + "-family" +
              std::to_string(camfam);
  is_good_ = Open(camid, camfam, color_mode, display_mode, frame_rate);
  if (!is_good_) {
    printf("Cannot open UEye camera\n");
    return;
  }
  Run();
}

UEyeCapture::~UEyeCapture() { Release(); }

bool UEyeCapture::Open(int camid, CameraFamily::ID camfam, INT cl_mode,
                       HWND display_mode, SenseScalar frame_rate) {
  camera_family_ = camfam;
  color_mode_ = cl_mode;
  int error = IS_SUCCESS;
  camera_ = (HIDS)camid;

  // if more than one camera present, show SelectCamera dialog
  int ncameras = 0;
  is_GetNumberOfCameras(&ncameras);

  if (ncameras == 0) {
    printf("no UEyeInterface camera connected\n");
    return false;
  }
  error = is_InitCamera(&camera_, display_mode);
  ///  is_SetHwnd( m_hCam, hWnd );

  if (error != IS_SUCCESS) {
    if (error == IS_NO_USB20) {
      printf("can not open camera, no usb 2.0 bus\n");
    } else if (error == IS_NOT_CALIBRATED) {
      printf("can not open camera, camera not callibrated\n");
    } else if (error == IS_ALL_DEVICES_BUSY) {
      printf("can not open camera, all devices busy\n");
    } else {
      printf("ca not open camera\n");
    }

    camera_ = NULL;
    return false;
  }
  CAMINFO ci;
  if (is_GetCameraInfo(camera_, &ci) != IS_SUCCESS) {
    printf(" Could not get sensor info from the camera\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  } else {
    PrintCameraInfo(ci);
  }

  SENSORINFO si;
  if (is_GetSensorInfo(camera_, &si) != IS_SUCCESS) {
    printf(" Could not get sensor info from the camera\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  } else {
    PrintSensorInfo(si);
  }

  GetMaxImageSize(si, &frame_width_, &frame_height_);

  printf("camera open, reseting all parameters to default\n");
  is_ResetToDefault(camera_);

  if (InitDisplayMode(si) != IS_SUCCESS) {
    printf(" Could not initialize display mode\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  };

  Init();

  // start live video
  error = is_CaptureVideo(camera_, IS_WAIT);

  printf("Camera initialized\n");
  return (error == IS_SUCCESS);
}

bool UEyeCapture::Open(int camid, std::string const &param_file,
                       CameraFamily::ID camfam, INT cl_mode,
                       HWND display_mode) {
  camera_family_ = camfam;
  color_mode_ = cl_mode;
  int error = IS_SUCCESS;
  camera_ = (HIDS)camid;

  // if more than one camera present, show SelectCamera dialog
  int ncameras = 0;
  is_GetNumberOfCameras(&ncameras);

  if (ncameras == 0) {
    printf("no UEyeInterface camera connected\n");
    return false;
  }
  error = is_InitCamera(&camera_, display_mode);
  ///  is_SetHwnd( m_hCam, hWnd );

  if (error != IS_SUCCESS) {
    if (error == IS_NO_USB20) {
      printf("can not open camera, no usb 2.0 bus\n");
    } else if (error == IS_NOT_CALIBRATED) {
      printf("can not open camera, camera not callibrated\n");
    } else if (error == IS_ALL_DEVICES_BUSY) {
      printf("can not open camera, all devices busy\n");
    } else {
      printf("ca not open camera\n");
    }

    camera_ = NULL;
    return false;
  }
  CAMINFO ci;
  if (is_GetCameraInfo(camera_, &ci) != IS_SUCCESS) {
    printf(" Could not get sensor info from the camera\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  } else {
    PrintCameraInfo(ci);
  }
#ifdef WIN32
  std::wstring stemp = string2wstring(param_file);
  LPCWSTR unicode_ca = stemp.c_str();
#else
  const char *unicode_ca = param_file.c_str();
#endif
  error = is_ParameterSet(camera_, IS_PARAMETERSET_CMD_LOAD_FILE,
                          (void *)unicode_ca, NULL);
  if (error != IS_SUCCESS) {
    printf("camera initialization failed loading %s\n", param_file);
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  }

  SENSORINFO si;
  if (is_GetSensorInfo(camera_, &si) != IS_SUCCESS) {
    printf(" Could not get sensor info from the camera\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  } else {
    PrintSensorInfo(si);
  }

  GetMaxImageSize(si, &frame_width_, &frame_height_);

  if (InitDisplayMode(si) != IS_SUCCESS) {
    printf(" Could not initialize display mode\n");
    is_ExitCamera(camera_);
    camera_ = NULL;
    return false;
  };

  Init();

  // start live video
  error = is_CaptureVideo(camera_, IS_WAIT);

  printf("Camera initialized\n");
  return (error == IS_SUCCESS);
}

void UEyeCapture::GetMaxImageSize(SENSORINFO &sensor_info, unsigned *width,
                                  unsigned *height) {
  // Check if the camera supports an arbitrary AOI
  // Only the ueye xs does not support an arbitrary AOI
  INT aoi_supported = 0;
  BOOL b_aoi_supported = true;
  if (is_ImageFormat(camera_, IMGFRMT_CMD_GET_ARBITRARY_AOI_SUPPORTED,
                     (void *)&aoi_supported,
                     sizeof(aoi_supported)) == IS_SUCCESS) {
    b_aoi_supported = (aoi_supported != 0);
  }

  if (!b_aoi_supported) {
    // All other sensors, Get maximum image size
    *width = sensor_info.nMaxWidth;
    *height = sensor_info.nMaxHeight;
  } else {
    // Only ueye xs, Get image size of the current format
    IS_SIZE_2D imageSize;
    is_AOI(camera_, IS_AOI_IMAGE_GET_SIZE, (void *)&imageSize,
           sizeof(imageSize));

    *width = imageSize.s32Width;
    *height = imageSize.s32Height;
  }
}

int UEyeCapture::InitDisplayMode(SENSORINFO const &si) {
  int error = IS_NO_SUCCESS;

  if (camera_ == NULL)
    return IS_NO_SUCCESS;

  // if some image memory exist already then free it
  if (image_memory_ != NULL)
    error = is_FreeImageMem(camera_, image_memory_, memory_id_);

  image_memory_ = NULL;

  error = is_SetDisplayMode(camera_, IS_SET_DM_DIB);
  if (error == IS_SUCCESS) {
    int bpp = 8;
    switch (color_mode_) {
    case IS_CM_RGBA8_PACKED:
      bpp = 32;
      opencv_color_mode_ = CV_8UC4;
      byte_perpixel_ = 4;
      break;
    case IS_CM_RGB8_PACKED:
      bpp = 24;
      opencv_color_mode_ = CV_8UC3;
      byte_perpixel_ = 3;
      break;
    default:
      printf("Color mode not supported by this UEyeCapture wrapper\n");
      return IS_NO_SUCCESS;
      break;
    }

    // allocate an image memory.
    error = is_AllocImageMem(camera_, frame_width_, frame_height_, bpp,
                             &image_memory_, &memory_id_);
  } // end if(nRet == IS_SUCCESS)

  if (error == IS_SUCCESS) {
    // set the image memory only for the bitmap mode when allocated
    error = is_SetImageMem(camera_, image_memory_, memory_id_);

    // set the desired color mode
    error = is_SetColorMode(camera_, color_mode_);
    // set the image size to capture
    IS_SIZE_2D imageSize;
    imageSize.s32Width = frame_width_;
    imageSize.s32Height = frame_height_;

    error = is_AOI(camera_, IS_AOI_IMAGE_SET_SIZE, (void *)&imageSize,
                   sizeof(imageSize));
  }
  return error;
}

//! read latest frame
EventPtr UEyeCapture::CaptureEvent() {
  if (!camera_ || !is_good_)
    return ImageEventPtr();
  if (is_FreezeVideo(camera_, IS_WAIT) == IS_SUCCESS) {
    cv::Mat frame(frame_height_, frame_width_, opencv_color_mode_,
                  image_memory_);

    // memcpy(frame.data, image_memory_,
    // frame_width_*frame_height_*byte_perpixel_);
    auto t = std::chrono::high_resolution_clock::to_time_t(
        std::chrono::high_resolution_clock::now());
    ;
    return ImageEventPtr(new ImageEvent(t, frame));
  } else {
    printf(" UEye error capturing the image\n");
  }
  return ImageEventPtr();
}

/// proper shutdown for the camera
void UEyeCapture::Release() {
  is_good_ = false;
  if (camera_ != 0) {
    // Disable messages
    is_EnableMessage(camera_, IS_FRAME, NULL);

    // Stop live video
    is_StopLiveVideo(camera_, IS_WAIT);

    // Free the allocated buffer
    if (image_memory_ != NULL)
      is_FreeImageMem(camera_, image_memory_, memory_id_);

    image_memory_ = NULL;

    // Close camera
    is_ExitCamera(camera_);
    camera_ = NULL;
  }
}

HCAM &UEyeCapture::GetCamera() { return camera_; }
} // namespace sense