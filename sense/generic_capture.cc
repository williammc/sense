// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/generic_capture.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <map>
#include <string>
#include <iostream>
#include "sense/tracking_events.h"

namespace sense {
GenericCapture::GenericCapture(std::string const &filename)
    : SensePublisher(filename), controllable_(false), hdr_support_(false) {
  is_good_ = VideoCapture::open(filename);
  if (!is_good_) {
    std::cerr << "GenericCapture cannot open file " << filename << std::endl;
    return;
  }
  Run();
}

GenericCapture::GenericCapture(int deviceid, CameraFamily::ID id)
    : controllable_(false), hdr_support_(false) {
  identity_ = "genericcapture-id" + std::to_string(id) + "-framily" +
              std::to_string(id);
  bool is_good_ = VideoCapture::open(deviceid);
  if (!is_good_) {
    std::cerr << "GenericCapture cannot open device #" << deviceid << std::endl;
    return;
  }
  Init();
  Run();
}

GenericCapture::~GenericCapture() {}

void GenericCapture::Init() {
  // list all possible parameter for the supported camera library
  map_string_paramid_[std::string("Brightness")] = CV_CAP_PROP_BRIGHTNESS;
  map_string_paramid_[std::string("Exposure")] = CV_CAP_PROP_EXPOSURE;
  map_string_paramid_[std::string("Sharpness")] = 0;
  map_string_paramid_[std::string("WhiteBalance")] = 0;
  map_string_paramid_[std::string("HUE")] = CV_CAP_PROP_HUE;
  map_string_paramid_[std::string("Saturation")] = CV_CAP_PROP_SATURATION;
  map_string_paramid_[std::string("Gamma")] = 0;
  map_string_paramid_[std::string("Shutter")] = 0;
  map_string_paramid_[std::string("Gain")] = CV_CAP_PROP_GAIN;
  map_string_paramid_[std::string("Iris")] = 0;
  map_string_paramid_[std::string("Focus")] = 0;
  map_string_paramid_[std::string("Zoom")] = 0;
  map_string_paramid_[std::string("Pan")] = 0;
  map_string_paramid_[std::string("Tilt")] = 0;
  map_string_paramid_[std::string("FrameRate")] = CV_CAP_PROP_FPS;
  int count = 0;
  for (auto tpair : map_string_paramid_) {
    if (tpair.second > 0) {
      SenseScalar value = get(tpair.second);
      if (value > 0) {
        count++;
        map_string_paramid_[tpair.first] = tpair.second;
        parameter_.map_string_paramvalue[tpair.first] =
            static_cast<unsigned int>(value);
        parameter_.map_string_auto[tpair.first] = true;
        parameter_.map_paramid_minmax[tpair.first] =
            std::make_pair(0, 1000); // arbitrary max
      }
    }
  }
  controllable_ = (count > 0);
  new_param_ = false;
}

bool GenericCapture::IsGood() { return is_good_; }

EventPtr GenericCapture::CaptureEvent() {
  if (new_param_.load()) {
    parameter_ = toset_param_;
    for (auto tpair : parameter_.map_string_paramvalue)
      set(map_string_paramid_[tpair.first], tpair.second);
    new_param_.store(false);
  }
  cv::Mat img;
  VideoCapture::read(img);
  if (!img.empty()) {
    cv::cvtColor(img, img, CV_BGR2RGB);
    auto t =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    return ImageEventPtr(new ImageEvent(t, img));
  }
  return ImageEventPtr();
}

unsigned int GenericCapture::GetProperty(std::string const &prop_name) const {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0)
    return parameter_.map_string_paramvalue.at(prop_name);
  return 0;
}

bool GenericCapture::SetProperty(std::string const &prop_name,
                                 unsigned int value) {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0) {
    toset_param_ = parameter_;
    toset_param_.map_string_paramvalue[prop_name] = value;
    new_param_.store(true);
  }
  return true;
}

} // namespace sense