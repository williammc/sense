#pragma once

#include "xsens/xsensor.h"
#include "sense/sense_manager.h"

namespace sense {

class XSENSCapture : public SensePublisher {
public:
  XSENSCapture() = delete;
  XSENSCapture(const XSENSCapture&) = delete;
  XSENSCapture(std::string usb_port) {
    openned_ = false;
    if (!xsens_.Open(usb_port, CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT,
                               CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX)) {
      std::cout << "Cannot open port " <<  usb_port  << std::endl;
      return;
    }
    openned_ = true;
    Run();
  }

  ~XSENSCapture() { xsens_.Close(); }

  EventPtr CaptureEvent() {
    if (!openned_) return EventPtr();
    double t;
    int counter;
    std::array<float, 3> acc, gyr, mag, euler;
    std::array<float, 4> quad;
    std::array<float, 9> last_matrix;
    xsens_.GetMeasurement(counter, acc, gyr, mag,
                          euler, quad, last_matrix, t);

    return InertialEventPtr(new InertialEvent(t, gyr, acc, mag, quad));
  }

protected:
  bool openned_;
  xsens::XSensor xsens_;
};
}  // namespace sense