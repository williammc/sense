#pragma once
#include <chrono>
#include <stdint.h>
#include <opencv2/core/core.hpp>

#include "sense/sense_api.h"

namespace sense {
/// get microseconds timestamp
inline double Timestamp() {
    auto t = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(
               t.time_since_epoch()).count();
}

// Useful opencv related ======================================================
// C = [A | B]
inline void CombineMatrixes(const cv::Mat &mat1, const cv::Mat &mat2,
                            cv::Mat &mResult) {
  mResult = cv::Mat(std::max<int>(mat1.rows, mat2.rows),
                    mat1.cols + mat2.cols, mat1.type());
  cv::Mat mat1ROI(mResult, cv::Rect(0, 0, mat1.cols, mat1.rows));
  mat1.copyTo(mat1ROI);
  cv::Mat mat2ROI(mResult, cv::Rect(mat1.cols, 0, mat2.cols, mat2.rows));
  mat2.copyTo(mat2ROI);
}

// Useful depth image functions ===============================================
// Calculates depth histogram (auto figure out depth resolution if given 0)
bool SENSE_API CalcDepthHistogram(const cv::Mat& depth_img,
                         std::vector<float>& depth_hist,
                         int depth_resolution = 0);

// Generate histogram image from depth16bit data
void SENSE_API GenerateHistogramImage(const cv::Mat& depth16_img,
                              cv::Mat& histogram_img);

// Converts depth image (16bit) to gray image 8bit (kinda compression)
void SENSE_API ConvertDepth16bitDepth8bit(const cv::Mat& depth16bit_img,
                                  cv::Mat& depth8bit_img);

}  // namespace sense
