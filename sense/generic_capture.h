// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <iostream>
#include <map>
#include <memory>
#include <string>

#include <opencv2/highgui/highgui.hpp>

#include "sense/datatypes.h"
#include "sense/event_queue.h"
#include "sense/sense_manager.h"

namespace sense {

// add camera families when neccessary
struct CameraFamily {
enum ID : unsigned {
  GENERIC = 0,
  PGR_MIN,
  PGR_FIREFLY_MV,
  PGR_DRAGON,
  PGR_MAX,
  LOGITECH_MIN,
  LOGITECH_WEBCAM,
  LOGITECH_PRO,
  LOGITECH_MAX
};
};

// GenericParameter to hold essential camera parameters if possible
struct GenericParameter {
  GenericParameter() {}

  void print_parameter(std::string const& property_name) {
    std::pair<unsigned int, unsigned int> minmax = 
                                              map_paramid_minmax[property_name];
    printf("%s: in [%u, %u]\n", property_name.c_str(), 
                                minmax.first, minmax.second);
  }

  std::map<std::string, bool> map_string_auto;
  std::map<std::string, unsigned int> map_string_paramvalue;
  std::map<std::string, std::pair<unsigned int, unsigned int> > map_paramid_minmax;
};

// to form a base for common capturing API
class GenericCapture : public cv::VideoCapture, public SensePublisher {
 public:
  GenericCapture() = default;
  GenericCapture(int deviceid, CameraFamily::ID cam_family = CameraFamily::GENERIC);
  GenericCapture(const GenericCapture&) = delete;
  GenericCapture(std::string const& filename);
  virtual ~GenericCapture();

  virtual void Init();

  virtual bool IsGood();

  virtual EventPtr CaptureEvent();

  bool IsHDRSupported() { return hdr_support_; }

  virtual bool SetHDR(bool) { return false; }

  bool IsControllable() { return controllable_; }

  virtual unsigned int GetProperty(std::string const& prop_name) const;
  virtual bool SetProperty(std::string const& prop_name, unsigned int value);
  virtual void Release() {}

  GenericParameter& parameter() { return parameter_; }

 protected:
  bool is_good_;
  GenericParameter parameter_;
  GenericParameter toset_param_;
  std::atomic<bool> new_param_;   ///< indicating new param set
  std::map<std::string, int> map_string_paramid_;
  bool controllable_;
  bool hdr_support_;
};
}  // namespace sense