// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <atomic>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>
#include "signalx/signalx.h"
#include "sense/common_macros.h"
#include "sense/event_queue.h"
#include "sense/sense_api.h"

namespace sense {
FORWARD_DECLARE_STRUCT_SHARED_PTR(Event);
FORWARD_DECLARE_STRUCT_SHARED_PTR(EventQueue);
FORWARD_DECLARE_STRUCT_SHARED_PTR(SensePublisher);
FORWARD_DECLARE_STRUCT_SHARED_PTR(SenseSubscriber);
FORWARD_DECLARE_STRUCT_SHARED_PTR(SenseManager);

using EventSignal = sigx::Signal<void(EventPtr)>;

/// Publisher is to 'sense' & collect specific sensor data =====================
struct SENSE_API SensePublisher {
  SensePublisher(const SensePublisher&) = delete;
  virtual ~SensePublisher() { Stop(); }

  SensePublisher(std::string id = "noid") : stop_(false), identity_(id) {}

  virtual EventPtr CaptureEvent() = 0;

  virtual void Run() {
    auto looping_func = [&]() {
      while (!stop_) {
        auto evt = CaptureEvent();
        if (evt) {
          std::lock_guard<std::mutex> lock(sync_);
          evt_signal_(evt);
        } else {
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
      }
    };
    this_thread_ = std::thread(looping_func);
  }

  virtual void Reset(){};

  void Stop() {
    stop_ = true;
    if (this_thread_.joinable())
      this_thread_.join();
  }

  void DisconnectAll() {
    evt_signal_.disconnect_all();
  }

  std::string identity() const { return identity_; }

  EventSignal& Signal() { return evt_signal_; }

 protected:
  friend SenseSubscriber;

  EventSignal evt_signal_;
  std::string identity_;

  std::atomic<bool> stop_;
  std::mutex sync_;
  std::thread this_thread_;
};

inline bool SENSE_API
operator==(const SensePublisher& lhs, const SensePublisher& rhs) {
  return lhs.identity() == rhs.identity();
}

/// Subscriber is for receiving sensor data from Publisher =====================
struct SENSE_API SenseSubscriber : public EventQueue, public sigx::Observer {
  SenseSubscriber(unsigned queue_limit = 500) { queue_limit_ = queue_limit; }

  virtual ~SenseSubscriber() {
    DeregisterAll();
  }

  void Register(SensePublisher& pub) {
    pub.Signal().connect<SenseSubscriber, &SenseSubscriber::OnEventCallback>(
        this);
    pubs_.push_back(&pub);
  }

  virtual void OnEventCallback(EventPtr evt) {
    push(evt);
    while (size() >= queue_limit_) pop();
  }

  virtual void Reset() {}

  void DeregisterAll() {
    for (auto pub : pubs_) {
      pub->evt_signal_.disconnect<SenseSubscriber,
                                  &SenseSubscriber::OnEventCallback>(*this);
    }
    pubs_.clear();
  }

 protected:
  unsigned queue_limit_;
  std::vector<SensePublisher*> pubs_;
};

/// Subscriber & Publisher is for receiving sensor data and send data ==========
struct SENSE_API SensePublishRetailer : public SensePublisher,
                                            sigx::Observer {
  using SensePublisher::sync_;
  SensePublishRetailer(unsigned queue_limit = 1000) {
    queue_limit_ = queue_limit;
  }

  virtual ~SensePublishRetailer() {}

  virtual EventPtr CaptureEvent() override { return EventPtr(); }

  virtual void Run() override {}

  void Register(SensePublisher& pub) {
    pub.Signal()
        .connect<SensePublishRetailer,
                 &SensePublishRetailer::OnEventCallback>(this);
  }

  virtual void OnEventCallback(EventPtr evt) {
    std::lock_guard<std::mutex> lock(sync_);
    evt_signal_(evt);
  }

  virtual void Reset() {}

 protected:
  unsigned queue_limit_;
};

/// For managing sensor data publisher and Subscriber ==========================
struct SENSE_API SenseManager {
  SenseManager() {}
  virtual ~SenseManager() { clear(); }

  void clear() {
    subscribers_.clear();
    id_publisher_.clear();
  }

  bool Register(SensePublisherPtr npub) {
    if (id_publisher_.count(npub->identity()) > 0) return false;
    id_publisher_[npub->identity()] = npub;
    return true;
  }

  bool Register(SenseSubscriberPtr rec, SensePublisherPtr pub) {
    if (id_publisher_.count(pub->identity()) == 0) {
      id_publisher_[pub->identity()] = pub;
      rec->Register(*pub);
      return false;
    }

    rec->Register(*pub);
    subscribers_.push_back(rec);
    return true;
  }

  bool Register(SenseSubscriberPtr rec, const std::string& pub_id) {
    if (id_publisher_.count(pub_id) == 0) {
      printf("Publisher id#%s not registered in this manager\n",
             pub_id.c_str());
      return false;
    }

    rec->Register(*id_publisher_[pub_id]);
    subscribers_.push_back(rec);
    return true;
  }

 protected:
  std::unordered_map<std::string, SensePublisherPtr> id_publisher_;
  std::vector<SenseSubscriberPtr> subscribers_;
};
}  // namespace sense