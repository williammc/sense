// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <dc1394/dc1394.h>
#include <sensor/generic_capture.h>

namespace sense {
class DC1394Capture : public GenericCapture {
 public:
  DC1394Capture() = delete;
  DC1394Capture(const DC1394Capture&) = delete;
  DC1394Capture(int device, 
                CameraFamily::ID camfam=CameraFamily::GENERIC,
                dc1394video_mode_t video_mode = DC1394_VIDEO_MODE_640x480_MONO8,
                dc1394framerate_t frame_rate = DC1394_FRAMERATE_30);
  ~DC1394Capture();

  bool Open(int device, CameraFamily::ID camfam=CameraFamily::GENERIC,
            dc1394video_mode_t video_mode = DC1394_VIDEO_MODE_640x480_MONO8,
            dc1394framerate_t frame_rate = DC1394_FRAMERATE_30);
  void Init() override;
  // read latest frame
  EventPtr CaptureEvent() override;
  // useful method to setup HDR capturing function (works with FireFly cameras)
  bool SetHDR(bool on) override;
  unsigned int GetProperty(std::string const& prop_name) override;
  bool SetProperty(std::string const& prop_name, unsigned int value) override;

  // proper shutdown for the camera
  void Release() override;

  dc1394camera_t& camera() { return *camera_; }

 protected:
  unsigned frame_width_;
  unsigned frame_height_;
  CameraFamily::ID camera_family_;
  dc1394_t* context_;
  dc1394camera_t* camera_;
  dc1394video_mode_t video_mode_;
  std::map<std::string, dc1394feature_t> map_string_paramid_;
};
}//namespace sense