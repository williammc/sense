#include "sense/image_writer.h"
#include <iomanip>
#include <iostream>
#include <thread>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace sense {

inline std::string WriteImageToFile(const cv::Mat& img, const std::string& name) {
  std::vector<int> jpg_params;
  jpg_params.push_back(cv::IMWRITE_JPEG_QUALITY);
  jpg_params.push_back(100);
  cv::Mat outimg;
  if (img.type() == CV_8UC3)
    cv::cvtColor(img, outimg, CV_RGB2BGR);
  else
    outimg = img;

  cv::imwrite(name, outimg, jpg_params);
  return name;
}


void WaitForWriterToEnd(ImageWriter **writer) {
  if (*writer != nullptr) {
    std::cout << "still saving ..  " << std::endl;
    while ((*writer)->imagestored_counter < (*writer)->imagependding_counter) {
      std::cout << (*writer)->imagestored_counter << "of "
                << (*writer)->imagependding_counter
                << "are saved" << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    (*writer)->Stop();
  }
}

bool StoreImageCommon(ImageWriter **writer, 
                      const cv::Mat& frame,
                      const std::string output_path,
                      std::string& filename,
                      const std::string &type,
                      const std::string& prefix) {
  if (*writer == nullptr) {
    *writer = new ImageWriter(output_path, frame.size());
  }

  std::ostringstream name;
  name << prefix << "_" << std::setw(6) << std::setfill('0')
       << (*writer)->imagependding_counter << "-" << type << ".png";
  filename = name.str();

  int index = ((*writer)->start_index + 1) % int((*writer)->image_buffer.size());
  if (index != (*writer)->end_index) {
    (*writer)->filename_buffer[(*writer)->start_index] = filename;
    frame.copyTo((*writer)->image_buffer[(*writer)->start_index]);
    (*writer)->start_index = index;
    (*writer)->imagependding_counter += 1;
    return true;
  } else {
    printf("drop!\n");
    return false;
  }
}

ImageWriter::ImageWriter (const std::string & path, const cv::Size & size,
               const unsigned buffer_size)
    : writer_path(path),
      imagependding_counter(0),
      imagestored_counter(0),
      image_buffer(buffer_size),
      filename_buffer(buffer_size),
      start_index(0),
      end_index(0),
      stopped(false) {
  Run();
  }

ImageWriter::~ImageWriter() {
  Stop();
}

void ImageWriter::Run() {
    auto thread_func = [&]() {
      while (!stopped) {
        if (end_index == start_index) {
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
          continue;
        }
        printf("Writer:: saving image to %s, imagestored_counter:%d, end_index:%d\n",
               (writer_path + "/" + filename_buffer[end_index]).c_str(),
               imagestored_counter, end_index);
        imagestored_counter += 1;
        WriteImageToFile(image_buffer[end_index],
                         writer_path + "/" + filename_buffer[end_index]);
        end_index = (end_index + 1) % int(image_buffer.size());
      }
    };

    this_thread_ = std::thread(thread_func);
  }

void ImageWriter::Stop() {
  stopped = true;
}

}  // namespace sense