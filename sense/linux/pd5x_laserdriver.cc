#include "sense/pd5x_laserdriver.h"

#include <chrono>
#include <fstream>
#include <fcntl.h>
#include <iostream>
#include <termios.h>

#include "sense/util.h"

using namespace std;
namespace sense {

// bool CompareString2CharArray(std::string const& s, char* ca) {
//   for (int i = 0; i < s.length(); ++i) {
//     if (s.at(i) != ca[i])
//       return false;
//   }
//   return true;
// }

// bool check_command(HANDLE const& filehandle, std::string const& command,
//                   std::string const& exp_reply, double timeout =
//                   10) {
//   int res = write(filehandle, command.c_str(), command.length());
//   if (res < 0) {
//     std::cout << "LaserDriver:: Cannot write '"<< command << "' to serial port
//     res:" << res << std::endl;
//     return false;
//   } else {
//     std::this_thread::sleep_for(std::chrono::milliseconds(100));
//     string buffer;
//     buffer.resize(exp_reply.length());
//     res = read(filehandle, &buffer.at(0), exp_reply.length()); //Reply: '0 0
//     0 0 Reply'
//     int inner_count=0;
//     while (res < exp_reply.length() && inner_count < 10) {
//       inner_count++;
//       std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
//       int inner_res = read(filehandle, &buffer.at(res),
//       exp_reply.length()-res);
//       if (inner_res > 0)
//         res += inner_res;
//     }
//     if (res < exp_reply.length() ||
//         !CompareString2CharArray(exp_reply, &buffer.at(0))) {
//       cout << "LaserDriver:: Oops command '"<<
//               command << "' to serial port reply:" << buffer << std::endl;
//       return false;
//     }
//   }
//   return true;
// }

// given a command, check if the reply is correct
bool check_command(HANDLE const& filehandle, std::string const& command,
                   std::string const& exp_reply, const int timeout = 10) {
  int res = write(filehandle, command.c_str(), command.length());
  if (res < 0) {
    std::cout << "Cannot write '" << command << "' to serial port res:" << res
         << std::endl;
    return false;
  } else {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    string buffer;
    buffer.resize(exp_reply.length() + 3);
    res = read(filehandle, &buffer.at(0), exp_reply.length());
    int inner_count = 0;
    while (res < exp_reply.length() && inner_count < 10) {
      inner_count++;
      std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
      int inner_res =
          read(filehandle, &buffer.at(res), exp_reply.length() - res);
      if (inner_res > 0) res += inner_res;
    }
    if (buffer.find_first_of(exp_reply) == string::npos) {
      printf("LaserDriver:: Oops command '%s' to serial port reply:'%s'",
             command.c_str(), buffer.c_str());
      return false;
    }
  }
  return true;
}

PD5xLaserdriver::~PD5xLaserdriver() {
  this->close();
  Stop();
}

void PD5xLaserdriver::close() {
  if (closed_)
    return;
  Stop();

  if (measuremode_ == MULTIPLE_EXTEND) {
    cout << "LaserDriver:: Stop Multiple Measurement Extended" << std::endl;
    check_command(filehandle_, "0 0 setMonVal;", "2 0 0 Reply");
    check_command(filehandle_, "8 0 setMonVal;", "2 0 8 Reply");
    check_command(filehandle_, "2 -1 -1 doMeasDistExt;", "0 0 0 0 Reply");
  } else if (measuremode_ == MULTIPLE) {
    cout << "Stop Multiple Measurement" << std::endl;
    check_command(filehandle_, "stopTracking;", "0 0 Reply;");
  }

  SetMeasuringMode(TARGETTING);
  //  check_command(filehandle_, "switchMeasOff;", "0 0 Reply");
  ::close(filehandle_);
  closed_ = true;
}

EventPtr PD5xLaserdriver::CaptureEvent() {
  if (closed_) return EventPtr();
  if (change_mode_.load()) {
    if (measuremode_ == MULTIPLE) {
      string command = "stopTracking;";  // command code for targetting
      int res = write (filehandle_, command.c_str(), command.length());
      if (res < 0) {
        printf("Cannot write 'stopTracking;' to serial port:%d\n", res);
        return EventPtr();
      }
    }
    measuremode_ = new_mode_;
    b_needreset_ = true;
    change_mode_.store(false);
  }

  double d_timeout = 10 * 1.e-3;  // convert to micro-seconds (as in ScopedTimer)
  auto start_time = std::chrono::steady_clock::now();
  int res = 0;
  string command = "";
  if (measuremode_ == TARGETTING) {
    std::cout << " Targetting" << std::endl;
    b_needreset_ = true;
  } else if (measuremode_ == SINGLE) {
    std::cout << "LaserDriver:: Single Measurement" << std::endl;
    command = "1 doMeasDist;";  // command code for single measurement
    res = write(filehandle_, command.c_str(), command.length());
    b_needreset_ = true;
  } else if (measuremode_ == MULTIPLE) {
    if (b_needreset_) {
      b_needreset_ = false;
      std::cout << "LaserDriver:: Multiple Measurement" << std::endl;
      command = "1 startTracking;";  // command code for multiple measurement
      res = write(filehandle_, command.c_str(), command.length());
      if (res < 0) {
        std::cout << "LaserDriver:: Cannot write '1 startTracking;' to serial port "
                "res:" << res << std::endl;
        return LaserEventPtr();
      } else {
        std::this_thread::sleep_for(std::chrono::milliseconds(30));
        res = read(filehandle_, ca_buffer_, 10);
      }
    }
  } else if (measuremode_ == SINGLE_EXTEND) {
    std::cout << "LaserDriver:: Single Measurement Extended" << std::endl;
    command = "0 -1 -1 doMeasDistExt;";  // command code for single measurement
    res = write(filehandle_, command.c_str(), command.length());
  } else if (measuremode_ == MULTIPLE_EXTEND) {
    if (b_needreset_) {
      b_needreset_ = false;
      std::cout << "LaserDriver:: Multiple Measurement Extended" << std::endl;
      res = check_command(filehandle_, "1 -1 -1 doMeasDistExt;",
                          ">>\n  0 0 0 0 Reply");
      if (res < 0) {
        std::cout << "LaserDriver:: Cannot write '1 -1 -1 doMeasDistExt;' to serial "
                "port res:" << res << std::endl;
        return LaserEventPtr();
      } else {
        if (!check_command(filehandle_, "0 1 setMonVal;", "\n2 0 0 Reply"))
          return LaserEventPtr();
        if (!check_command(filehandle_, "8 1 setMonVal;", "\n2 0 8 Reply"))
          return LaserEventPtr();
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        res = read(filehandle_, ca_buffer_, 10);
      }
    }
  }

  if (b_needreset_) std::cout << " writing to serial port res:" << res << std::endl;
  if (cached_bytes_.size() > 0) {
    if (measuremode_ == SINGLE || measuremode_ == SINGLE_EXTEND) {
      cached_bytes_.clear();
    } else if (measuremode_ == MULTIPLE || measuremode_ == MULTIPLE_EXTEND) {
      while (!cached_bytes_.empty()) {
        cached_bytes_.erase(cached_bytes_.begin());
        if (!cached_bytes_.empty()) {
          if (cached_bytes_.at(0) == 'M') break;
        }
      }
    }
  }
  // read distance result
  float laser_dist = 0;
  res = -1;
  int iter = 0;
  auto current_time = start_time;
  bool keep_checking = true;
  int error_code = -1;
  int scanf_return = 0;
  while (keep_checking) {
    current_time = std::chrono::steady_clock::now();
    if (d_timeout > 0 && std::chrono::duration_cast<std::chrono::microseconds>(current_time- start_time).count() > d_timeout)
      break;
    keep_checking++;
    res = read(filehandle_, ca_buffer_,
               max_readout_bytes);  // read bytes (maximum possible)
    if (res > 0) {
      iter++;
      for (int i = 0; i < res; ++i) cached_bytes_.push_back(ca_buffer_[i]);
      bool pattern_ok = false;
      if (measuremode_ == SINGLE) {
        scanf_return = sscanf((char*)&cached_bytes_.front(), "2 %d %f Reply",
                              &error_code, &laser_dist);
        pattern_ok = scanf_return == 2;
      } else if (measuremode_ == SINGLE_EXTEND) {
        // Reply: '2 0 1844364 324 Reply'. 2nd number is error code, 3rd number
        // is distance in um, 4th number is measurement signal
        scanf_return = sscanf((char*)&cached_bytes_.front(), "2 %d %f %f Reply",
                              &error_code, &laser_dist);
        laser_dist *= 1.e-6;
        pattern_ok = scanf_return == 2;
      } else if (measuremode_ == MULTIPLE) {
        if (cached_bytes_[0] != 'M') return LaserEventPtr();
        char temp1, temp2;
        scanf_return = sscanf((char*)&cached_bytes_.front(), "M: 8 %d 0 %f%cM%c",
                              &error_code, &laser_dist, &temp1, &temp2);
        pattern_ok = (scanf_return == 4);
      } else if (measuremode_ == MULTIPLE_EXTEND) {
        if (cached_bytes_[0] != 'M') return LaserEventPtr();
        char temp1, temp2;
        scanf_return = sscanf((char*)&cached_bytes_.front(), "M: 8 %d 0 %f%cM%c",
                              &error_code, &laser_dist, &temp1, &temp2);
        laser_dist *= 1.e-6;
        pattern_ok = (scanf_return == 4);
      }
      if (pattern_ok || iter >= 3) keep_checking = false;
    }
    if (keep_checking)
      std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }

  if (laser_dist <= 0.0 || error_code != 0) {
    std::cout << "===================" << std::endl;
    std::cout << "LaserDriver:: Cannot read Distance from serial port res:" << res
         << std::endl;
    std::cout << "distance:" << laser_dist << " error_code:" << error_code
         << " scanf_return:" << scanf_return << " iter:" << iter << std::endl;
    for (int i = 0; i < cached_bytes_.size(); ++i) {
      if (i > 0 & cached_bytes_[i] == 'M') break;
      std::cout << cached_bytes_[i];
    }
    std::cout << "===================" << std::endl;
    return LaserEventPtr();
  }
  return LaserEventPtr(new LaserEvent(Timestamp(), laser_dist));
}

bool PD5xLaserdriver::OpenAndConfig(std::string s_portpath) {
  std::cout << "LaserDriver::  Opening serial port:" << s_portpath << std::endl;
  filehandle_ = ::open(s_portpath.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (filehandle_ < 0) {
    std::cout << "LaserDriver::  Cannot open serial port" << std::endl;
    return false;
  }
  struct termios serial_io;
  tcgetattr(filehandle_, &serial_io);  // save current port settings
  serial_io.c_cflag =
      CS8 | CREAD | CLOCAL;  // 8n1, see termios.h for more information
  cfsetospeed(&serial_io, B115200);
  cfsetispeed(&serial_io, B115200);
  tcflow(filehandle_, CRTSCTS);
  tcsetattr(filehandle_, TCSANOW, &serial_io);

  std::cout << "LaserDriver::  FileHandle:" << filehandle_ << std::endl;

  std::cout << "LaserDriver::  wakes up the laser device ..." << std::endl;
  string command = ";";
  int res = write(filehandle_, command.c_str(), command.length());
  if (res < 0) {
    std::cout << "LaserDriver:: Cannot write ';' to serial port:" << res << std::endl;
    return false;
  } else {
    res = read(filehandle_, ca_buffer_, 5);
  }

  command = "ping;";
  res = write(filehandle_, command.c_str(), command.length());
  if (res < 0) {
    std::cout << "LaserDriver:: Cannot write 'ping;' to serial port:" << res << std::endl;
    return false;
  } else {
    res = read(filehandle_, ca_buffer_, 10);
  }
  return true;
}

bool PD5xLaserdriver::SetMeasuringMode(MeasuringMode mode) {
  if (mode == measuremode_) return false;
  if (measuremode_ == MULTIPLE) {
    string command = "stopTracking;";  // command code for targetting
    int res = write(filehandle_, command.c_str(), command.length());
    if (res < 0) {
      std::cout << "LaserDriver:: Cannot write 'stopTracking;' to serial port:"
           << res << std::endl;
      return false;
    }
  }
  measuremode_ = mode;
  b_needreset_ = true;
  return true;
}
}  // namespace sense
