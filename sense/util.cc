#include "sense/util.h"

namespace sense {

// Utilities for Depth image ===================================================
// Calculates depth histogram (auto figure out depth resolution if given 0)
bool CalcDepthHistogram(const cv::Mat& depth_img,
                                 std::vector<float>& depth_hist,
                                 int depth_resolution) {
  if (depth_img.empty())
    return false;
  auto depth_data = (uint16_t*)depth_img.data;
  unsigned int npoints = 0;
  if (depth_resolution == 0) {
    for (int y = 0; y < depth_img.rows; ++y) {
      for (int x = 0; x < depth_img.cols; ++x, ++depth_data) {
        if (*depth_data != 0) {
          depth_resolution = std::max(depth_resolution, int(*depth_data));
        }
      }
    }
  }

  if (depth_resolution == 0)
    return false;

  depth_hist.resize(depth_resolution + 1);
  depth_data = (uint16_t*)depth_img.data;
  for (int y = 0; y < depth_img.rows; ++y) {
    for (int x = 0; x < depth_img.cols; ++x, ++depth_data) {
      if (*depth_data != 0) {
        depth_hist[*depth_data]++;
        npoints++;
      }
    }
  }
  for (int index = 1; index < depth_resolution; index++) {
    depth_hist[index] += depth_hist[index-1];
  }
  if (npoints) {
    for (int index=1; index < depth_resolution; index++) {
      depth_hist[index] = (unsigned int)(256 * (1.0f - (depth_hist[index] / npoints)));
    }
  }
  return true;
}

// Generate histogram image from depth16bit data
void GenerateHistogramImage(const cv::Mat& depth16_img,
                                     cv::Mat& histogram_img) {
  if (histogram_img.empty())
    histogram_img.create(depth16_img.rows, depth16_img.cols, CV_8UC3);

  std::vector<float> depth_hist;
  if (!CalcDepthHistogram(depth16_img, depth_hist))
    return;

  auto depth_data = (uint16_t*)depth16_img.data;
  auto hist_data = (uint8_t*)histogram_img.data;
  for (int r = 0; r < histogram_img.rows; ++r) {
    for (int c = 0; c < histogram_img.cols; ++c, ++depth_data) {
      if (*depth_data != 0) {
        *(hist_data++) = 0;
        *(hist_data++) = depth_hist[*depth_data];
        *(hist_data++) = depth_hist[*depth_data];
      } else {
        *(hist_data++) = 0;
        *(hist_data++) = 0;
        *(hist_data++) = 0;
      }
    }
  }
}

// Converts depth image (16bit) to gray image 8bit (kinda compression)
void ConvertDepth16bitDepth8bit(const cv::Mat& depth16bit_img,
                                cv::Mat& depth8bit_img) {
  std::vector<float> depth_hist;
  if (depth8bit_img.empty())
    depth8bit_img.create(depth16bit_img.rows,
                         depth16bit_img.cols,
                         CV_8UC1);
  depth8bit_img.setTo(cv::Scalar(0));
  if (!CalcDepthHistogram(depth16bit_img, depth_hist))
    return;
  uint16_t* depth_data = (uint16_t*)depth16bit_img.data;
  for (int r = 0; r < depth8bit_img.rows; ++r) {
    for (int c = 0; c < depth8bit_img.cols; ++c, ++depth_data) {
      if (*depth_data != 0) {
        depth8bit_img.at<uint8_t>(r, c) = depth_hist[*depth_data];
      }
    }
  }
}
}  // namespace sense