#include "sense/openni2_capture.h"
#include <iostream>
#include "sense/tracking_events.h"

namespace sense {
  OpenNI2Capture::OpenNI2Capture(bool sync_capture, bool mirror)
    : sync_capture_(sync_capture), mirror_images_(mirror), streams_(nullptr) {
    need_manual_mirror_ = false;
    is_good_ = Init();
    if (!is_good_) {
      std::cerr << "OpenNI2Capture cannot find any Kinect/PrimeSense sensors"
        << std::endl;
      return;
    }
    Run();
  }

  OpenNI2Capture::~OpenNI2Capture() {
    Stop();
    bool need_shd = false;
    if (streams_ != nullptr) {
      need_shd = true;
      streams_[0]->stop();
      streams_[0]->destroy();
      streams_[1]->stop();
      streams_[1]->destroy();
      delete[]streams_;
    }
    if (device_.isValid()) {
      device_.close();
      if (need_shd)
        openni::OpenNI::shutdown();
    }
  }

  template<typename T>
  inline void flip_vertical(cv::Mat& img) {
    for (int r = 0; r < img.rows; ++r) {
      for (int c = 0; c < img.cols; ++c) {
        img.at<T>(r, c) = img.at<T>(r, img.cols - 1 - c);
      }
    }
  }

  EventPtr OpenNI2Capture::CaptureEvent() {
    cv::Mat cimg, dimg;
    double t;
    if (sync_capture_) {
      if (SyncCapture(t, cimg, dimg)) {
        if (need_manual_mirror_) {
          cv::flip(cimg, cimg, 1);
          cv::flip(dimg, dimg, 1);
        }
        cv::Mat tdimg;
        if (dimg.cols != 640)
          cv::resize(dimg, tdimg, cv::Size(640, 480), 0, 0, cv::INTER_NEAREST);
        else
          tdimg = dimg;
        cv::Mat tcimg;
        if (cimg.cols != 640)
          cv::resize(cimg, tcimg, cv::Size(640, 480), 0, 0, cv::INTER_NEAREST);
        else
          tcimg = cimg;
        return ColorAndDepthEventPtr(new ColorAndDepthEvent(t, cimg, tdimg, true));
      }

      return ColorAndDepthEventPtr();
    }

    if (CaptureColor(t, cimg)) {
      if (need_manual_mirror_) {
        cv::flip(cimg, cimg, 1);
      }
      return ImageEventPtr(new ImageEvent(t, cimg));
    }

    if (CaptureDepth(t, dimg)) {
      if (need_manual_mirror_) {
        cv::flip(dimg, dimg, 1);
      }
      return DepthImageEventPtr(new DepthImageEvent(t, dimg));
    }

    return EventPtr();
  }

  double OpenNI2Capture::GetColorHorizontalFieldOfView() const {
    if (!is_good_) return 0.0;
    return color_stream_.getHorizontalFieldOfView();
  }

  double OpenNI2Capture::GetColorVerticalFieldOfView() const {
    if (!is_good_) return 0.0;
    return color_stream_.getVerticalFieldOfView();
  }

  bool OpenNI2Capture::Init() {
    openni::Status rc = openni::STATUS_OK;
    const char* device_uri = openni::ANY_DEVICE;
    rc = openni::OpenNI::initialize();

    printf("OpenNI2Capture Init:\n%s\n", openni::OpenNI::getExtendedError());

    rc = device_.open(device_uri);
    if (rc != openni::STATUS_OK) {
      printf("Device open failed:\n%s\n", openni::OpenNI::getExtendedError());
      openni::OpenNI::shutdown();
      return false;
    }

    auto search_and_setmode = [](const openni::SensorInfo* sinfo, int width, int height, int pixelformat,
                                 openni::VideoStream& stream) -> bool {
      openni::Status rc = openni::STATUS_ERROR;
      const openni::Array< openni::VideoMode>& dmodes = sinfo->getSupportedVideoModes();
      printf("List modes\n");
      for (int i = 0; i < dmodes.getSize(); ++i) {
        auto mode = dmodes[i];
        printf("%i: %ix%i, %i fps, %i format\n", i, mode.getResolutionX(), mode.getResolutionY(),
               mode.getFps(), mode.getPixelFormat());
      }
      for (int i = 0; i < dmodes.getSize(); ++i) {
        auto mode = dmodes[i];
        printf("%i: %ix%i, %i fps, %i format\n", i, mode.getResolutionX(), mode.getResolutionY(),
               mode.getFps(), mode.getPixelFormat());
        if (mode.getResolutionX() == width && mode.getResolutionY() == height &&
            mode.getPixelFormat() == pixelformat) {
          rc = stream.setVideoMode(mode);
          break;
        }
      }

      if (rc != openni::STATUS_OK) {
        printf("Couldn't set mode for given stream:\n%s\n", openni::OpenNI::getExtendedError());
        stream.destroy();
        return false;
      }
      printf("Set mode resolution(%i, %i)\n", width, height);
      return true;
    };

    const openni::SensorInfo* dsinfo = device_.getSensorInfo(openni::SENSOR_DEPTH);
    rc = depth_stream_.create(device_, openni::SENSOR_DEPTH);
    if (rc == openni::STATUS_OK) {
      if (depth_stream_.setMirroringEnabled(mirror_images_) != openni::STATUS_OK) {
        printf("Couldn't mirror depth stream:\n%s\n", openni::OpenNI::getExtendedError());
        need_manual_mirror_ = true;
      }

      rc = depth_stream_.start();
      if (rc != openni::STATUS_OK) {
        printf("Couldn't start depth stream:\n%s\n", openni::OpenNI::getExtendedError());
        depth_stream_.destroy();
        return false;
      }

    } else {
      printf("Couldn't find depth stream:\n%s\n", openni::OpenNI::getExtendedError());
      return false;
    }

    const openni::SensorInfo* sinfo = device_.getSensorInfo(openni::SENSOR_COLOR);
    rc = color_stream_.create(device_, openni::SENSOR_COLOR);
    if (rc == openni::STATUS_OK) {
      if (!search_and_setmode(sinfo, 640, 480, openni::PIXEL_FORMAT_RGB888,
        color_stream_))
        return false;

      if (color_stream_.setMirroringEnabled(mirror_images_) != openni::STATUS_OK) {
        printf("Couldn't mirror color stream:\n%s\n", openni::OpenNI::getExtendedError());
        need_manual_mirror_ = true;
      }
      rc = color_stream_.start();
      if (rc != openni::STATUS_OK) {
        printf("Couldn't start color stream:\n%s\n", openni::OpenNI::getExtendedError());
        color_stream_.destroy();
        return false;
      }

    } else {
      printf("Couldn't find color stream:\n%s\n", openni::OpenNI::getExtendedError());
      return false;
    }

    if (!depth_stream_.isValid() || !color_stream_.isValid()) {
      printf("No valid streams_. Exiting\n");
      openni::OpenNI::shutdown();
      return false;
    }

    device_.setDepthColorSyncEnabled(sync_capture_);
    device_.setImageRegistrationMode(openni::ImageRegistrationMode::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

    streams_ = new openni::VideoStream*[2];
    streams_[0] = &depth_stream_;
    streams_[1] = &color_stream_;

    depth_stream_.stop();
    if (!search_and_setmode(dsinfo, 640, 480, openni::PIXEL_FORMAT_DEPTH_1_MM,
      depth_stream_))
      return false;
    if (depth_stream_.start() != openni::STATUS_OK) {
      printf("Couldn't start depth stream:\n%s\n", openni::OpenNI::getExtendedError());
    }

    return true;
  }

  bool OpenNI2Capture::SyncCapture(double& timestamp,
                                   cv::Mat& rgb_img,
                                   cv::Mat& depth_img) {
    if (!device_.isValid()) return false;
    auto get_frame_func = [&](int index) -> bool {
      switch (index) {
      case 0:
        depth_stream_.readFrame(&depth_frame_);
        timestamp = depth_frame_.getTimestamp();
        return depth_frame_.isValid();
      case 1:
        color_stream_.readFrame(&color_frame_);
        timestamp = color_frame_.getTimestamp();
        return color_frame_.isValid();
      default:
        printf("Error in wait\n");
        return false;
      }
    };

    int changed_index;
    openni::Status rc = openni::OpenNI::waitForAnyStream(streams_, 2, &changed_index);
    if (rc != openni::STATUS_OK) {
      printf("Wait failed\n");
      return false;
    }

    if (!get_frame_func(changed_index)) return false;

    int another_index = changed_index;
    int count = 0;
    while ((another_index == changed_index) && (count < 10)) {
      rc = openni::OpenNI::waitForAnyStream(streams_, 2, &another_index);
      count++;
    }

    if (another_index == changed_index) return false;

    if (!get_frame_func(another_index)) return false;

    // copy color data
    if (rgb_img.empty())
      rgb_img.create(color_frame_.getHeight(), color_frame_.getWidth(), CV_8UC3);
    auto color_pixels = (const openni::RGB888Pixel*)color_frame_.getData();
    int row_size = color_frame_.getStrideInBytes() / sizeof(openni::RGB888Pixel);
    auto data = (openni::RGB888Pixel*)rgb_img.data;
    for (int r = 0; r < rgb_img.rows; ++r) {
      memcpy(data, color_pixels, rgb_img.cols*sizeof(openni::RGB888Pixel));
      data += rgb_img.cols;
      color_pixels += row_size;
    }

    // copy depth data
    if (depth_img.empty())
      depth_img.create(depth_frame_.getHeight(), depth_frame_.getWidth(), CV_16UC1);
    auto depth_pixels = (const openni::DepthPixel*)depth_frame_.getData();
    row_size = depth_frame_.getStrideInBytes() / sizeof(openni::DepthPixel);
    auto ddata = (openni::DepthPixel*)depth_img.data;
    for (int r = 0; r < depth_img.rows; ++r) {
      memcpy(ddata, depth_pixels, depth_img.cols*sizeof(openni::DepthPixel));
      ddata += depth_img.cols;
      depth_pixels += row_size;
    }

    return true;
  }

  bool OpenNI2Capture::CaptureColor(double& timestamp, cv::Mat& rgb_img) {
    if (!device_.isValid()) return false;
    int changed_index = -1;
    int count = 0;
    while ((changed_index != 1) && (count < 10)) {
      openni::Status rc = openni::OpenNI::waitForAnyStream(streams_, 2, &changed_index);
      count++;
    }

    color_stream_.readFrame(&color_frame_);
    timestamp = color_frame_.getTimestamp();

    if (!color_frame_.isValid()) return false;

    // copy color data
    if (rgb_img.empty())
      rgb_img.create(color_frame_.getHeight(), color_frame_.getWidth(), CV_8UC3);
    auto color_pixels = (const openni::RGB888Pixel*)color_frame_.getData();
    int row_size = color_frame_.getStrideInBytes() / sizeof(openni::RGB888Pixel);
    auto data = (openni::RGB888Pixel*)rgb_img.data;
    for (int r = 0; r < rgb_img.rows; ++r) {
      memcpy(data, color_pixels, rgb_img.cols*sizeof(openni::RGB888Pixel));
      data += rgb_img.cols;
      color_pixels += row_size;
    }

    return true;
  }


  bool OpenNI2Capture::CaptureDepth(double& timestamp, cv::Mat& depth_img) {
    if (!device_.isValid()) return false;
    int changed_index = -1;
    int count = 0;
    while ((changed_index != 0) && (count < 10)) {
      openni::Status rc = openni::OpenNI::waitForAnyStream(streams_, 2, &changed_index);
      count++;
    }

    depth_stream_.readFrame(&depth_frame_);
    timestamp = depth_frame_.getTimestamp();

    if (!depth_frame_.isValid()) return false;

    // copy depth data
    if (depth_img.empty())
      depth_img.create(depth_frame_.getHeight(), depth_frame_.getWidth(), CV_8UC3);
    auto depth_pixels = (const openni::DepthPixel*)depth_frame_.getData();
    int row_size = depth_frame_.getStrideInBytes() / sizeof(openni::DepthPixel);
    auto ddata = (openni::DepthPixel*)depth_img.data;
    for (int r = 0; r < depth_img.rows; ++r) {
      memcpy(ddata, depth_pixels, depth_img.cols*sizeof(openni::DepthPixel));
      ddata += depth_img.cols;
      depth_pixels += row_size;
    }

    return true;
  }
}  // namespace sense