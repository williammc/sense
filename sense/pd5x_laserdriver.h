// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <chrono>
#include <functional>
#include "sense/event_queue.h"
#include "sense/sense_api.h"
#include "sense/sense_manager.h"
#include "sense/tracking_events.h"

#define INFINITE_RANGE 11171  // infinite range

#ifdef WIN32
#include <windows.h>
#else
typedef int HANDLE;
typedef unsigned char UCHAR;
typedef short DWORD;
#endif

namespace sense {

inline std::ostream& operator << (std::ostream& lhs,
                                  LaserEvent const& data) {
  lhs << "Measurement Data " << std::endl
      << " timestamp: " << data.timestamp() << std::endl
      << " distance: " << data.distance() << std::endl;
  return lhs;
}

struct SENSE_API PD5xLaserdriver : public SensePublisher {
  enum MeasuringMode : unsigned {SINGLE = 0,
                                 MULTIPLE,
                                 TARGETTING,
                                 SINGLE_EXTEND,
                                 MULTIPLE_EXTEND};

  PD5xLaserdriver() = delete;
  PD5xLaserdriver(const PD5xLaserdriver&) = delete;

  // formal constructor
  PD5xLaserdriver(std::string portpath) {
    auto open = [&]() -> bool {
      measuremode_ = SINGLE;
      b_needreset_ = true;
      b_newarrival_ = false;
      cached_bytes_.reserve(2 * max_readout_bytes);
      if (!OpenAndConfig(portpath))
        return false;
      return true;
    };

    closed_ = false;
    measuremode_ = SINGLE;
    b_needreset_ = true;
    b_newarrival_ = false;
    Run();
  }

  ~PD5xLaserdriver();

  virtual void close();

  // timeout in miliseconds
  void Reset() {}
  EventPtr CaptureEvent();
  virtual bool OpenAndConfig(std::string portpath);
  // set measuring mode: single or multiple
  virtual bool SetMeasuringMode(MeasuringMode mode);

 protected:
  static const int BUFFERSIZE = 255;
  unsigned char ca_buffer_[BUFFERSIZE];
  const static int max_readout_bytes = 20;

  // used to cached extra bytes readed-out from the previous measurement
  std::vector<char> cached_bytes_;
  MeasuringMode measuremode_;

  std::atomic<bool> change_mode_;
  MeasuringMode new_mode_;
  
  bool closed_;
  HANDLE filehandle_;
  bool b_needreset_;  // multiple mode doesn't need to reset command
  bool b_newarrival_;  // new measurement result added
};

}  // namespace sense