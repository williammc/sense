// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/dc1394_capture.h"
#include "sense/generic_capture.h"

#include <opencv2/imgproc/imgproc.hpp>

namespace sense {

static bool IsVideoModeFrameRateSupported(dc1394camera_t* camera, dc1394video_mode_t video_mode,
                                          dc1394framerate_t frame_rate) {
  // check video mode
  dc1394video_modes_t video_modes;
  if(dc1394_video_get_supported_modes (camera, &video_modes)!= DC1394_SUCCESS) {
    printf("Failed on dc1394_video_get_supported_modes");
    return false;
  }
  bool vmode_support = false;
  for (unsigned int i = 0; i < video_modes.num; i++)
    if (video_mode == video_modes.modes[i]) {
      vmode_support = true;
    }
  if (!vmode_support)
    return false;

  dc1394framerates_t framerates;
  if (dc1394_video_get_supported_framerates(camera,video_mode,&framerates)
      != DC1394_SUCCESS) {
    printf(("Could not query supported framerates"));
    return false;
  }
  for (unsigned int i = 0; i < framerates.num; i++)
    if (frame_rate == framerates.framerates[i]) {
      return true;
    }
  return false;
}

// camera firmware dependent code
static bool GenericInitializeHDR(dc1394camera_t* camera, CameraFamily::ID camera_family) {
  if (camera_family > CameraFamily::PGR_MIN && camera_family < CameraFamily::PGR_MAX) {
    const unsigned int hdr_ctrl = 0x1800;
    uint32_t reg_val;
    dc1394error_t error;
    if ((error = dc1394_get_register(camera, hdr_ctrl, &reg_val )) != DC1394_SUCCESS) {
      std::cout << "Cannot get HDR control register" << std::endl;
      return false;
    }
    if (!0x80000000 & reg_val) {
      std::cout << "HDR not supported" << std::endl;
      return false;
    }

    const unsigned int hdr_shutter1 = 0x1820;
    const unsigned int hdr_shutter2 = 0x1840;
    const unsigned int hdr_shutter3 = 0x1860;
    const unsigned int hdr_shutter4 = 0x1880;

    const unsigned int hdr_gain1 = 0x1824;
    const unsigned int hdr_gain2 = 0x1844;
    const unsigned int hdr_gain3 = 0x1864;
    const unsigned int hdr_gain4 = 0x1884;

    if ((error=dc1394_set_register(camera, hdr_shutter1, 0x000 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Shutter1" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_shutter2, 0x120 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Shutter2" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_shutter3, 0x240 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Shutter3" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_shutter4, 0x360 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Shutter4" << std::endl;
      return false;
    }

    if ((error=dc1394_set_register(camera, hdr_gain1, 0x000 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Gain1" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_gain2, 0x0E3 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Gain2" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_gain3, 0x1C6 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Gain3" << std::endl;
      return false;
    }
    if ((error=dc1394_set_register(camera, hdr_gain4, 0x2A9 )) != DC1394_SUCCESS) {
      std::cout << "Cannot set HDR Gain4" << std::endl;
      return false;
    }
    std::cout << "HDR registers initialized..." << std::endl;
    return true;
  }

  std::cout << "HDR not supported for this Camera Family" << std::endl;
  return false;
}

static bool GenericSetHDR(bool on, dc1394camera_t* camera, CameraFamily::ID camera_family) {
  dc1394error_t error;
  if (camera_family > CameraFamily::PGR_MIN && camera_family < CameraFamily::PGR_MAX) {
    const unsigned int hdr_ctrl = 0x1800;
    const unsigned int hdr_on = 0x82000000;
    const unsigned int hdr_off = 0x80000000;
    if((error = dc1394_set_register(camera, hdr_ctrl, on ? hdr_on : hdr_off )) !=DC1394_SUCCESS) {
      std::cout << "Cannot set HDR control on/off" << std::endl;
      return false;
    }
    return true;
  }
}

DC1394Capture::DC1394Capture(int device, CameraFamily::ID camfam,
                         dc1394video_mode_t video_mode,
                         dc1394framerate_t frame_rate) {
  is_good_ = Open(device, camfam, video_mode, frame_rate);
  if (!is_good_)
    return;
  Run();
}

DC1394Capture::~DC1394Capture() {}

bool DC1394Capture::Open(int device, CameraFamily::ID camfam,
                         dc1394video_mode_t video_mode,
                         dc1394framerate_t frame_rate) {
  camera_family_ = camfam;
  this->video_mode_ = video_mode;
  context_ = dc1394_new();
  dc1394error_t error;

  // Enumerate the cameras connected to the system.
  dc1394camera_list_t *camera_list = NULL;

  if ((error=dc1394_camera_enumerate(context_, &camera_list)) != DC1394_SUCCESS) {
    printf(("Camera enumerate"));
    return false;
  }

  if (camera_list->num == 0) {
    dc1394_camera_free_list(camera_list);
    printf(("No dc1394 cameras found.\n"));
    return false;
  } else {
    if (device+1 > (int)camera_list->num)
      printf("Selected caemra out of range");
    std::cout << "List of cameras:\n";
    for (unsigned int i = 0; i < camera_list->num; i++)
      std::cout << "    Camera: " << i << ": unit=" << camera_list->ids[i].unit
                << " guid="  << camera_list->ids[i].guid  << "\n";
  }
  camera_ = dc1394_camera_new(context_, camera_list->ids[device].guid);
  dc1394_camera_free_list(camera_list);

  if (!camera_) {
    printf(("Failed on dc1394_camera_new"));
    return false;
  }

  if (!IsVideoModeFrameRateSupported(camera_, video_mode, frame_rate))
    return false;

  dc1394switch_t is_iso_on;
  if ((error = dc1394_video_get_transmission(camera_, &is_iso_on)) != DC1394_SUCCESS)
    is_iso_on = DC1394_OFF;
  if (is_iso_on == DC1394_ON)
    dc1394_video_set_transmission(camera_, DC1394_OFF);

  std::cout << "Selected camera: " << camera_->vendor_id
            << ":" << camera_->model_id << "(guid: " << camera_->guid << ")\n";

  dc1394_camera_reset(camera_);

  // Selected mode and frame-rate; Now tell the camera to use these.
  // At the moment, hard-code the channel to speed 400. This is maybe something to fix in future?
  if (dc1394_video_set_iso_speed(camera_, DC1394_ISO_SPEED_400) != DC1394_SUCCESS) {
    printf(("Could not set ISO speed."));
    return false;
  }

  if (dc1394_video_set_iso_channel(camera_, 0) != DC1394_SUCCESS) {
    printf(("Could not set ISO channel."));
    return false;
  }

  if (dc1394_video_set_mode(camera_, video_mode) != DC1394_SUCCESS) {
    printf(("Could not set video mode"));
    return false;
  }

  if (dc1394_video_set_framerate(camera_, frame_rate) != DC1394_SUCCESS) {
    printf(("Could not set frame-rate"));
    return false;
  }

  // Hack to disable resource allocation -- see above
  // error = dc1394_capture_setup(mpLDCP->pCamera, 4, DC1394_CAPTURE_FLAGS_DEFAULT);
  if (dc1394_capture_setup(camera_, 4, 0) != DC1394_SUCCESS) {
    printf(("Could not setup capture."));
    return false;
  }

  if (dc1394_video_set_transmission(camera_, DC1394_ON) != DC1394_SUCCESS) {
    printf(("Could not start ISO transmission."));
    return false;
  }

  dc1394_get_image_size_from_video_mode(camera_, video_mode, &frame_width_, &frame_height_);

  init();
  return true;
}

void DC1394Capture::Init() {
  hdr_support_ = GenericInitializeHDR(camera_, camera_family_);
  //NOTE: list all necessary features needed for setup here
  map_string_paramid_[std::string("Brightness")] = DC1394_FEATURE_BRIGHTNESS;
  map_string_paramid_[std::string("Exposure")] = DC1394_FEATURE_EXPOSURE;
  map_string_paramid_[std::string("Sharpness")] = DC1394_FEATURE_SHARPNESS;
  map_string_paramid_[std::string("WhiteBalance")] = DC1394_FEATURE_WHITE_BALANCE;
  map_string_paramid_[std::string("HUE")] = DC1394_FEATURE_HUE;
  map_string_paramid_[std::string("Saturation")] = DC1394_FEATURE_SATURATION;
  map_string_paramid_[std::string("Gamma")] = DC1394_FEATURE_GAMMA;
  map_string_paramid_[std::string("Shutter")] = DC1394_FEATURE_SHUTTER;
  map_string_paramid_[std::string("Gain")] = DC1394_FEATURE_GAIN;
  map_string_paramid_[std::string("Iris")] = DC1394_FEATURE_IRIS;
  map_string_paramid_[std::string("Focus")] = DC1394_FEATURE_FOCUS;
  map_string_paramid_[std::string("Zoom")] = DC1394_FEATURE_ZOOM;
  map_string_paramid_[std::string("Pan")] = DC1394_FEATURE_PAN;
  map_string_paramid_[std::string("Tilt")] = DC1394_FEATURE_TILT;
  map_string_paramid_[std::string("FrameRate")] = DC1394_FEATURE_FRAME_RATE;

  dc1394error_t error;
  using namespace std;
  // display camera parameters' value
  for (std::map<string, dc1394feature_t>::iterator it = map_string_paramid_.begin();
      it!=map_string_paramid_.end(); ++it) {
    dc1394feature_t f = (*it).second;
    std::pair<unsigned int, unsigned int> minmax;
    error = dc1394_feature_get_boundaries(camera_,f, &minmax.first, &minmax.second);
    std::cout << "minmax:[" << minmax.first << "\t" << minmax.second << "]" << std::endl;
    if (minmax.second > 0) {
      map_string_paramid_[(*it).first] = (*it).second;
      parameter_.map_paramid_minmax[(*it).first] = minmax;
      parameter_.map_string_auto[(*it).first]=true;
      error = dc1394_feature_get_value(camera_, f, &parameter_.map_string_paramvalue[(*it).first]);
      parameter_.print_parameter((*it).first);
      dc1394switch_t on_off;
      error = dc1394_feature_get_power(camera_, f, &on_off);
      std::cout << "power:" << on_off << std::endl;
      if(!on_off)
        dc1394_feature_set_power(camera_, f, DC1394_ON);
    }
  }
  controllable_ = (parameter_.map_paramid_minmax.size()>0);
}

// read latest frame
EventPtr DC1394Capture::CaptureEvent() {
  dc1394video_frame_t *dc1394_frame;
  if (dc1394_capture_dequeue(camera_, DC1394_CAPTURE_POLICY_WAIT, &dc1394_frame) != DC1394_SUCCESS)
    throw(("Failed on deque"));
  if (frame.empty())
    frame.create(frame_height_, frame_width_, CV_8UC3);
  dc1394color_coding_t color_coding;
  dc1394byte_order_t byte_order;
  bool bayer_decode=false;
  int bayer_decode_bits=8;
  switch(video_mode_){
  case DC1394_VIDEO_MODE_640x480_MONO8:
  case DC1394_VIDEO_MODE_800x600_MONO8:
  case DC1394_VIDEO_MODE_1024x768_MONO8:
  case DC1394_VIDEO_MODE_1280x960_MONO8:
  case DC1394_VIDEO_MODE_1600x1200_MONO8:
    color_coding = DC1394_COLOR_CODING_MONO8;
    bayer_decode=true;
    bayer_decode_bits=8;
    break;
  case DC1394_VIDEO_MODE_640x480_MONO16:
  case DC1394_VIDEO_MODE_800x600_MONO16:
  case DC1394_VIDEO_MODE_1024x768_MONO16:
  case DC1394_VIDEO_MODE_1280x960_MONO16:
  case DC1394_VIDEO_MODE_1600x1200_MONO16:
    color_coding = DC1394_COLOR_CODING_MONO16;
    bayer_decode=true;
    bayer_decode_bits=16;
    break;
  case DC1394_VIDEO_MODE_640x480_YUV411:
    color_coding = DC1394_COLOR_CODING_YUV411;
    byte_order = DC1394_BYTE_ORDER_UYVY;
    break;
  case DC1394_VIDEO_MODE_320x240_YUV422:
  case DC1394_VIDEO_MODE_640x480_YUV422:
  case DC1394_VIDEO_MODE_800x600_YUV422:
  case DC1394_VIDEO_MODE_1024x768_YUV422:
  case DC1394_VIDEO_MODE_1280x960_YUV422:
    color_coding = DC1394_COLOR_CODING_YUV422;
    byte_order = DC1394_BYTE_ORDER_UYVY;
    break;
  case DC1394_VIDEO_MODE_160x120_YUV444:
    color_coding = DC1394_COLOR_CODING_YUV444;
    byte_order = DC1394_BYTE_ORDER_UYVY;
    break;
  case DC1394_VIDEO_MODE_640x480_RGB8:
  case DC1394_VIDEO_MODE_800x600_RGB8:
  case DC1394_VIDEO_MODE_1024x768_RGB8:
  case DC1394_VIDEO_MODE_1280x960_RGB8:
  case DC1394_VIDEO_MODE_1600x1200_RGB8:
    color_coding = DC1394_COLOR_CODING_RGB8;
    break;
  }

  if (bayer_decode && bayer_decode_bits == 8) {
    if(dc1394_bayer_decoding_8bit(dc1394_frame->image,(uint8_t*) frame.data, frame_width_, frame_height_,
                                  DC1394_COLOR_FILTER_RGGB, DC1394_BAYER_METHOD_HQLINEAR)!= DC1394_SUCCESS)
      throw(("Failed on decoding bayer to rgb"));
  } else if (bayer_decode && bayer_decode_bits == 16) {
    if (dc1394_bayer_decoding_16bit((uint16_t*)dc1394_frame->image,
                                    (uint16_t*) frame.data, frame_width_, frame_height_,
                                   DC1394_COLOR_FILTER_RGGB, DC1394_BAYER_METHOD_HQLINEAR, 24)!= DC1394_SUCCESS)
      throw (("Failed on decoding bayer to rgb"));
  } else {
    if (dc1394_convert_to_RGB8(dc1394_frame->image,(uint8_t*) frame.data, frame_width_, frame_height_,
                              byte_order, color_coding, 24) != DC1394_SUCCESS)
      throw (("Failed on converting image to rgb"));
  }

  if (dc1394_capture_enqueue(camera_, dc1394_frame) != DC1394_SUCCESS)
    throw(("Failed on enqueue"));

  auto t = std::chrono::high_resolution_clock::to_time_t(std::chrono::high_resolution_clock::now());;
  return ImageEventPtr(new ImageEvent(t, frame));
}

// useful method to setup HDR capturing function (works with FireFly cameras)
bool DC1394Capture::SetHDR(bool on) {
  if (!hdr_support_)
    return false;
  return GenericSetHDR(on, camera_, camera_family_);
}

unsigned int DC1394Capture::GetProperty(std::string const& prop_name) {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0)
    return parameter_.map_string_paramvalue[prop_name];
  return 0;
}

bool DC1394Capture::SetProperty(std::string const& prop_name, unsigned int value) {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0) {
    if (value < parameter_.map_paramid_minmax[prop_name].first || value > parameter_.map_paramid_minmax[prop_name].second)
      return false;
    if (dc1394_feature_set_mode(camera_, map_string_paramid_[prop_name], DC1394_FEATURE_MODE_MANUAL)
        != DC1394_SUCCESS)
      return false;
    parameter_.map_string_auto[prop_name]=false;
    if (dc1394_feature_set_value(camera_, map_string_paramid_[prop_name], value) != DC1394_SUCCESS)
      return false;
    parameter_.map_string_paramvalue[prop_name] = value;
    return true;
  }
}

// proper shutdown for the camera
void DC1394Capture::Release() {
  try {
    if (hdr_support_)
      GenericSetHDR(false, camera_, camera_family_);

    if (camera_) {
      dc1394_video_set_transmission(camera_, DC1394_OFF);
      dc1394_capture_stop(camera_);
      dc1394_camera_set_power(camera_, DC1394_OFF);
      dc1394_camera_free(camera_);
    }

    if (context_)
      dc1394_free(context_);
  } catch (std::exception &e) {
    std::cout << "failed to release dc1394 camera" << std::endl;
  }
}
}  // namespace sense