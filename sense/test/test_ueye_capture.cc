#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "sense/tracking_events.h"
#include "sense/ueye_capture.h"

int main (int argc, char** argv) {
  printf("Usage:  ~/test_ueye_capture [camera_id] [config_file]\n");
  const int camid = (argc > 1) ? std::atoi(argv[1]) : 0;
  const std::string cfg =
      (argc > 2) ? argv[2]
                 : std::string(Look3D_ROOT) + "/data/ueye-640x480-60fps.ini";
  printf("Opening camera id:%d\n", camid);
  if (!cfg.empty())
    printf("Camera parameters config file:%s\n", cfg.c_str());

  sense::UEyeCapture cap(cfg, camid);

  sense::SenseSubscriber rec;
  rec.Register(cap);
  while(cv::waitKey(10) != 27 && cap.IsGood()) {
    while (!rec.empty()) {
      auto res = std::dynamic_pointer_cast<sense::ImageEvent>(rec.pop());
      if (res) {
        cv::Mat bgr;
        cv::cvtColor(res->frame(), bgr, CV_RGB2BGR);
        cv::imshow("Test UEyeCapture", bgr);
      }
    }
  }
  rec.DeregisterAll();
}