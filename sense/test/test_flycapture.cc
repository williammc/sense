#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "sense/tracking_events.h"
#include "sense/flycapture.h"

int main (int argc, char** argv) {
  printf("Usage:  ~/test_flycapture [camera_id]\n");
  const int camid = (argc > 1) ? std::atoi(argv[1]) : 0;
  printf("Opening camera id:%d\n", camid);

  sense::FlyCapture cap(camid);

  sense::SenseSubscriber rec;
  rec.Register(cap);
  while(cv::waitKey(20) != 27 && cap.IsGood()) {
    sense::ImageEventPtr res;
    while (!rec.empty()) {
      auto res = std::dynamic_pointer_cast<sense::ImageEvent>(rec.pop());
      if (res && !res->frame().empty()) {
        cv::Mat bgr;
        cv::cvtColor(res->frame(), bgr, CV_RGB2BGR);
        cv::imshow("Test FlyCapture", bgr);
      }
    }
  }
  rec.DeregisterAll();
}