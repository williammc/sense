// Copyright 2012, 2013, 2014 The LooK3D Authors. All rights reserved.
#include <iostream>
#include <chrono>

#include <xsens/xsensor.h>
#include <xsens/xsensor_util.h>
#include "sense/sense_manager.h"

#include "config.h"

using namespace std;
using namespace sense;

int main (int argc, char * argv[] ) {
  string configfile = (argc > 1) ? argv[1] : CONFIG_FILE;
  printf("loading config file from: %s\n", configfile.c_str());

  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(configfile, pt);

  SensorThreadPool sensor_threadpool;
  sensor_threadpool.config(&pt);

  try {
    bool stop = false;
    int k = 0;

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    while (cv::waitKey(10) != 27 && !stop && k < 1000) {
      k++;
      std::this_thread::sleep_for(std::chrono::milliseconds(3));
      std::queue<std::shared_ptr<Event> > temp_events;
      while (!sensor_threadpool.is_eventqueue_empty())
        temp_events.push(sensor_threadpool.pop_event());
      while (!temp_events.empty()) {
        std::shared_ptr<Event> temp_event = temp_events.front();
        temp_events.pop();
        ImageEvent * img_event = dynamic_cast<ImageEvent *>(temp_event.get());
        if (img_event) {
          cv::Mat m_bgr;
          cv::cvtColor(img_event->frame, m_bgr, cv::COLOR_RGB2BGR);
          cv::imshow("test sensor_threadpool", m_bgr);
        }
        InertialEvent* inner_event =
            dynamic_cast<InertialEvent *>(temp_event.get());
        if (inner_event) {
          printf("acc: %6.3f\t%6.3f\t%6.3f\n" , inner_event->accel[0],
              inner_event->accel[1], inner_event->accel[2]);

          printf("gyr: %6.3f\t%6.3f\t%6.3f\n" , inner_event->gyro[0],
              inner_event->gyro[1], inner_event->gyro[2]);

          printf("mag: %6.3f\t%6.3f\t%6.3f\n" , inner_event->magnet[0],
              inner_event->magnet[1], inner_event->magnet[2]);
        }
        LaserEvent* laser_event = dynamic_cast<LaserEvent *>(temp_event.get());
        if(laser_event) {
          printf("laser: dist:%6.3fm\t time:%6.3f\n" ,
                 laser_event->distance, laser_event->getTime());
        }
      }

#ifdef WIN32
      if (_kbhit())
#else
      if(kbhit())
#endif
        stop = true;
    }
  } catch (std::exception &e) {
    cout << "Oops: " << e.what() << endl;
    return -1;
  }
  sensor_threadpool.stop();
  exit(0);
}


