#include <fstream>
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include "sense/pd5x_laserdriver.h"

using namespace std;
using namespace sense;

int main (int argc, char** argv) {
  cout << "Usage:  ~/testlaser [serialport]" << endl;
  cout << "Start laser thread..." << endl;
  std::string s_laserport = /*"/dev/tty.usbserial-01U41QN4"; //*/ "//./COM4";
  if(argc>1)
    s_laserport = argv[1];
  PD5xLaserdriver laserdriver(s_laserport);
  laserdriver.SetMeasuringMode(PD5xLaserdriver::MULTIPLE);

  SenseSubscriber rec;
  rec.Register(laserdriver);

  laserdriver.Run();

  try {
    int count = 0;
    bool stop = false;
    while(!stop && count < 10000) {
//      switch(key) {
//      case 'e':
//        laserdriver.setMeasuringMode(LaserDriverPD5x::MULTIPLE_EXTEND);
//        break;
//      case 'm':
//        laserdriver.setMeasuringMode(LaserDriverPD5x::MULTIPLE);
//        break;
//      case 's':
//        laserdriver.setMeasuringMode(LaserDriverPD5x::SINGLE);
//        break ;
//      }
      cv::waitKey(10);
      count++;

      // read distance result
      while (!rec.empty()) {
        auto res = dynamic_cast<LaserEvent*>(rec.pop().get());
        if (res) {
          cout << "distance:" << res->distance() << endl;
          if (res->distance() <= 0 || res->distance() > 100)
            cout << "Wrong measurement:" << endl;
        }
      }

#ifdef WIN32
      if (_kbhit())
#else
      if(kbhit())
#endif
        stop = true;
    }
  } catch (std::exception &e) {
    cout << " Oops: " << e.what() << endl;
  }
  return 0;
}

