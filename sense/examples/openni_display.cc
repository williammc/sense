//! Generic video recording from a typical camera
#include <iostream>
#include <string>
#include <strstream>

// OpenCV lib
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/tracking_events.h"
#include "sense/openni2_capture.h"
#include "sense/util.h"

static std::string WINDOW_NAME = "OpenNI Display";

int main(int argc, char ** argv) {
  sense::OpenNI2Capture oni;

  sense::SenseSubscriber video_rec;
  video_rec.Register(oni);

  cv::namedWindow(WINDOW_NAME, CV_WINDOW_AUTOSIZE);
  bool stop = false;
  bool pausing = false;
  while (!stop) {
    sense::EventPtr evt;
    while (!video_rec.empty())
      evt = video_rec.pop();
    auto cdimgevt = std::dynamic_pointer_cast<sense::ColorAndDepthEvent>(evt);
    if (!pausing && cdimgevt) {
      if (cdimgevt->frame().empty())
        continue;
      cv::Mat bgr;
      cv::cvtColor(cdimgevt->frame(), bgr, CV_RGB2BGR);

      static cv::Mat hist_img;
      sense::GenerateHistogramImage(cdimgevt->depth_frame(), hist_img);

      static cv::Mat depth_color_img;
      sense::CombineMatrixes(hist_img, bgr, depth_color_img);
      cv::imshow(WINDOW_NAME, depth_color_img);

    }

    //  Key press handling
    char key = cv::waitKey(10);
    switch (key) {
    case 27:
      stop = true;
      break;
    case 'p':
      pausing = !pausing;
      break;
    }
  }  // end loop
  //video_rec.DeregisterAll();
}  // end main()