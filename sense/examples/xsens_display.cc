// Copyright 2014 The Sense Authors. All rights reserved.
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include <opencv2/highgui/highgui.hpp>

#include "xsens/xsensor.h"

using namespace std;

static bool stop = false;
static string xsens_device = "/dev/ttyUSB0";
static SE3 last_pose;

static Eigen::Vector3d gravity;
static Eigen::Vector3d magnetic;
static SE3 last_pose_est;

static SE3 trans_world2cam(SO3(Eigen::Vector3d(0, 0, 1),
                                   Eigen::Vector3d(0, 1, -1)),
                             Eigen::Vector3d(0, 0, 1));

//static XSensor xsens;    //! this causes problem with XSensor threading

void run_inertial_thread() {
  static int k = 0;
  cout << "start run " << endl;

  int counter;
  Eigen::Vector3d acc, mag, gyr, euler;
  Eigen::Vector4d quad;
  double t, firstTime;

  xsens::XSensor xsens;
  double before = time(NULL);
  if (!xsens.open(xsens_device,
                  CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT,
                  CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX)) {
    cout << "Cannot open port " <<  xsens_device  << endl;
    return;
  }
  double after =  time(NULL);
  Eigen::Matrix3d last_matrix;
  xsens.getMeasurement(counter, acc, gyr, mag,
                       euler, quad, last_matrix, firstTime );
  cout << counter << " " << firstTime << " " << firstTime - counter * 0.01 << endl;
  cout << acc.transpose() << gyr.transpose() << mag.transpose() << endl;

  while (!stop) {
    cout << "in run::loop"<< ++k << endl;

    xsens.getMeasurement(counter, acc, gyr, mag,
                         euler, quad, last_matrix, t);
    cout << "acc:" << acc.transpose()
         << "gyro:" << gyr.transpose()
         << "mag:" << mag.transpose() << endl;
    cout << last_matrix << endl;
    last_pose = SE3(SO3(last_matrix), Eigen::Vector3d(0, 0, 0));
    last_pose_est = SE3(SO3(ComputeOrientation(
                                  gravity.normalized(),
                                  acc.normalized(),
                                  magnetic.normalized(),
                                  mag.normalized())),
                          Eigen::Vector3d(0, 0, 0));
    std::this_thread::sleep_for(std::chrono::millisecconds(10));
  }
  xsens.close();
}

int main(int argc, char *argv[]){
  string configfile = (argc>1)?argv[1]:CONFIG_FILE;
  cout << " loading config file from: " << configfile << endl;

  boost::property_tree::ptree pt;
  boost::property_tree::ini_parser::read_ini(configfile, pt);

  xsens_device = pt.get<string>("sensor.device");

  gravity = pt.get<Eigen::Vector3d>("sensor_calibration.gravity");
  magnetic = pt.get<Eigen::Vector3d>("sensor_calibration.magnetic");

  std::shared_ptr<std::thread> inertial_thread(new std::thread(std::bind(&run_inertial_thread)));
  while (!stop) {
    if (cv::waitKey(20) == 27) {
      stop=true;
      inertial_thread->join();
    }
    cv::Mat visualizer_image(500, 500, CV_8UC3, cv::Scalar(0, 0, 0));

    drawXYZ(visualizer_image, trans_world2cam,
            cv::Scalar(255, 255, 255), cv::Scalar(255, 255, 255), cv::Scalar(255, 255, 255), 4);
    drawXYZ(visualizer_image, trans_world2cam*last_pose,
            cv::Scalar(0, 0, 200), cv::Scalar(0, 200, 0), cv::Scalar(200, 0, 0), 2);
    drawXYZ(visualizer_image, trans_world2cam*last_pose_est,
            cv::Scalar(0, 0, 150), cv::Scalar(0, 150, 0), cv::Scalar(150, 0, 0), 1);

    cv::imshow("Sensor Coordinates", visualizer_image);
  }
  return 0;
}

