// Copyright 2014 The LooK3D Authors. All rights reserved.
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <thread>

#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <look3d/sensor/xsens/xsensor.h>
#include <look3d/geometry/abs_orientation.h>
#include <look3d/math/se3.h>
#include <look3d/track/util.h>
#include <look3d/track/kalman_rotationfilter.h>

#include "config.h"
#include "helpers.h"

using namespace std;
using namespace sense;

static boost::property_tree::ptree pt;
static bool stop = false;
static string xsens_device = "/dev/ttyUSB0";
static SE3 last_sensorworld2sensor;

static Eigen::Vector3d gravity;
static Eigen::Vector3d magnetic;
static SE3 last_sensorworld2sensor_est, last_sensorworld2sensor_filter;
static KmanRotationFilter rotation_filter;

static SE3 trans_world2cam(SO3(Eigen::Vector3d(0, 0, 1),
                                   Eigen::Vector3d(0, 1, -1)),
                             Eigen::Vector3d(0, 0, 1));

void configCalibration() {
  rotation_filter.set_noise(pt.get<double>("sensor_calibration.rotation_noise", 0.1),
                            pt.get<double>("sensor_calibration.map_noise", 1),
                            pt.get<double>("sensor_calibration.rotation_vel_noise", 1),
                            pt.get<double>("sensor_calibration.bias_noise", 0.001),
                            pt.get<double>("sensor_calibration.accel_noise", 0.1),
                            pt.get<double>("sensor_calibration.gyro_noise", 0.1),
                            pt.get<double>("sensor_calibration.mag_noise", 0.1),
                            pt.get<double>("sensor_calibration.vision_noise", 0.001));

  gravity = pt.get<Eigen::Vector3d>("sensor_calibration.gravity");
  magnetic = pt.get<Eigen::Vector3d>("sensor_calibration.magnetic");

  rotation_filter.set_gravity(gravity.normalized());
  rotation_filter.set_magnetic(magnetic.normalized());
}

//static XSensor xsens;    //! this causes problem with XSensor threading

void run_inertial_thread() {
  static int k=0;
  cout << "start run " << endl;

  int counter;
  Eigen::Vector3d acc, mag, gyr, euler;
  Eigen::Vector4d quad;
  double t, firstTime;

  xsens::XSensor xsens;
  double before = time(NULL);
  if (!xsens.open(xsens_device, CMT_OUTPUTMODE_CALIB | CMT_OUTPUTMODE_ORIENT,
                  CMT_OUTPUTSETTINGS_ORIENTMODE_MATRIX)) {
    cout << "Cannot open port " <<  xsens_device  << endl;
    return;
  }
  double after =  time(NULL);
  Eigen::Matrix3d last_matrix;
  xsens.getMeasurement(counter, acc, gyr, mag,
                       euler, quad, last_matrix, firstTime );
  cout << counter << " " << firstTime << " " << firstTime - counter * 0.01 << endl;
  cout << acc.transpose() << gyr.transpose() << mag.transpose() << endl;

  try {
    while (!stop) {
      bool success = xsens.getMeasurement(counter, acc, gyr, mag,
                                          euler, quad, last_matrix, t);

      if (success) {
        last_sensorworld2sensor = SE3(SO3(last_matrix), Eigen::Vector3d(0,0,0));
        last_sensorworld2sensor_est =
            SE3(SO3(ComputeOrientation(gravity.normalized(),
                                           acc.normalized(),
                                           magnetic.normalized(),
                                           mag.normalized())),
                  Eigen::Vector3d(0, 0, 0));
        // transform measurements to camera frame
        const Eigen::Vector3d corrected_gyro = 1 * gyr;
        const Eigen::Vector3d corrected_accel = 1 * acc/ gravity.norm();
        const Eigen::Vector3d corrected_mag = 1 * mag / magnetic.norm();
        // if this is the first event after a reset, use this as start time
        static double time = -1;
        if (time < 0)
          time = t;
        // predict  state
        const double dt = (t - time)*1.e-6;  // in second unit
        rotation_filter.Predict(dt);
        // update filter
        rotation_filter.UpdateSensor(corrected_gyro, corrected_accel, corrected_mag);
        time = t;
      }
    }
  } catch(std::exception& e) {
    printf("Issue:: %s\n", e.what());
  }

  xsens.close();
}

int main(int argc, char *argv[]) {
  string configfile = (argc>1)?argv[1]:CONFIG_FILE;
  cout << " loading config file from: " << configfile << endl;
  boost::property_tree::ini_parser::read_ini(configfile, pt);

  xsens_device = pt.get<string>("sensor.device");
  SE3 world2sensorworld = SE3(SO3(pt.get<Eigen::Matrix3d>("sensor_calibration.world2sensorworld")),
                                  Eigen::Vector3d(0,0,0));

  configCalibration();
  std::shared_ptr<std::thread> inertial_thread(new std::thread(std::bind(&run_inertial_thread)));
  char c;
  cv::Mat visualizer_image(300, 300, CV_8UC3, cv::Scalar(0, 0, 0));
  try {
    while (!stop) {
      c = cv::waitKey(10);
      if (c == 27) {
        stop=true;
        inertial_thread->join();
      } else if (c == 'r') {
        rotation_filter.Reset();
      }
      last_sensorworld2sensor_filter = SE3(rotation_filter.get_globalrotation(),
                                             Eigen::Vector3d(0, 0, 0));

      visualizer_image.setTo(cv::Scalar(0, 0, 0));
      // sensor's world coordinates
      drawXYZ(visualizer_image, trans_world2cam, cv::Scalar(255,255,255), cv::Scalar(255,255,255), cv::Scalar(255,255,255), 4);

      // 'ground-truth' rotation get from MTx
      drawXYZ(visualizer_image, trans_world2cam*last_sensorworld2sensor*world2sensorworld, cv::Scalar(0,0,255), cv::Scalar(0,255,0), cv::Scalar(255,0,0), 4);
#if 1 // draw direct rotation estimated from acc & mag
      drawXYZ(visualizer_image, trans_world2cam*last_sensorworld2sensor_est*world2sensorworld, cv::Scalar(250,0,0), cv::Scalar(0,250,0), cv::Scalar(0,0,250), 3);
#endif

      // filtered rotation
      drawXYZ(visualizer_image, trans_world2cam*last_sensorworld2sensor_filter*world2sensorworld, cv::Scalar(255,255,0), cv::Scalar(255,0,255), cv::Scalar(0,255,255), 2);

      cv::imshow("Sensor Coordinates", visualizer_image);
    }
  } catch (std::exception& e) {
    printf("Issue in main(): %s\n", e.what());
  }

  return 0;
}


