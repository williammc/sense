// Copyright 2014 The Sense Authors. All rights reserved.
#include <fstream>
#include "sense/sense_manager.h"
#include "sense/tracking_events.h"

namespace sense {
struct ImageEvent;
class ImageWriter;
class SensorRecorder : public SenseSubscriber {
 public:
  SensorRecorder(const std::string& filename);
  ~SensorRecorder();

  virtual void OnEventCallback(EventPtr evt);
  void toggle_recording() {
    recording_ = !recording_;
  }

 protected:
  bool StoreImageEvent(const ImageEvent* evt,
                       const std::string& tag,
                       const std::string& type = "color");

  bool recording_;
  std::ofstream logfile_;
  std::string prefix_;
  std::string output_path_;

  ImageWriter *image_writer_;
  ImageWriter* depth_writer_;
  ImageWriter* fused_depth_writer_;
};
}  // namespace sense