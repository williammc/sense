#pragma once

#define FORWARD_DECLARE_STRUCT_SHARED_PTR(T)          \
struct T;                              \
typedef std::shared_ptr<T> T##Ptr;

#define FORWARD_DECLARE_CLASS_SHARED_PTR(T)          \
class T;                                             \
typedef std::shared_ptr<T> T##Ptr;
