// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/flycapture.h"
#include "sense/tracking_events.h"

namespace sense {
static void PrintCameraInfo(FlyCapture2::CameraInfo *pCamInfo) {
  printf("\n*** CAMERA INFORMATION ***\n"
         "Serial number - %u\n"
         "Camera model - %s\n"
         "Camera vendor - %s\n"
         "Sensor - %s\n"
         "Resolution - %s\n"
         "Firmware version - %s\n"
         "Firmware build time - %s\n\n",
         pCamInfo->serialNumber, pCamInfo->modelName, pCamInfo->vendorName,
         pCamInfo->sensorInfo, pCamInfo->sensorResolution,
         pCamInfo->firmwareVersion, pCamInfo->firmwareBuildTime);
}

// camera firmware dependent code
static bool GenericInitializeHDR(FlyCapture2::Camera *camera,
                                 CameraFamily::ID camera_family_) {
  using namespace FlyCapture2;
  if (camera_family_ > CameraFamily::PGR_MIN &&
      camera_family_ < CameraFamily::PGR_MAX) {
    const unsigned int hdr_ctrl = 0x1800;
    unsigned int reg_val;
    Error error;
    if ((error = camera->ReadRegister(hdr_ctrl, &reg_val)) != PGRERROR_OK) {
      std::cout << "Cannot get HDR control register" << std::endl;
      return false;
    }
    if (!0x80000000 & reg_val) {
      std::cout << "HDR not supported" << std::endl;
      return false;
    }

    const unsigned int hdr_shutter1 = 0x1820;
    const unsigned int hdr_shutter2 = 0x1840;
    const unsigned int hdr_shutter3 = 0x1860;
    const unsigned int hdr_shutter4 = 0x1880;

    const unsigned int hdr_gain1 = 0x1824;
    const unsigned int hdr_gain2 = 0x1844;
    const unsigned int hdr_gain3 = 0x1864;
    const unsigned int hdr_gain4 = 0x1884;

    if ((error = camera->WriteRegister(hdr_shutter1, 0x000)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Shutter1" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_shutter2, 0x120)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Shutter2" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_shutter3, 0x240)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Shutter3" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_shutter4, 0x360)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Shutter4" << std::endl;
      return false;
    }

    if ((error = camera->WriteRegister(hdr_gain1, 0x000)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Gain1" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_gain2, 0x0E3)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Gain2" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_gain3, 0x1C6)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Gain3" << std::endl;
      return false;
    }
    if ((error = camera->WriteRegister(hdr_gain4, 0x2A9)) != PGRERROR_OK) {
      std::cout << "Cannot set HDR Gain4" << std::endl;
      return false;
    }
    std::cout << "HDR registers initialized..." << std::endl;
    return true;
  }

  std::cout << "HDR not supported for this Camera Family" << std::endl;
  return false;
}

static bool GenericSetHDR(bool on, FlyCapture2::Camera *camera,
                          CameraFamily::ID camera_family) {
  if (camera_family > CameraFamily::PGR_MIN &&
      camera_family < CameraFamily::PGR_MAX) {
    const unsigned int hdr_ctrl = 0x1800;
    const unsigned int hdr_on = 0x82000000;
    const unsigned int hdr_off = 0x80000000;
    FlyCapture2::Error error;
    if ((error = camera->WriteRegister(hdr_ctrl, on ? hdr_on : hdr_off)) !=
        FlyCapture2::PGRERROR_OK) {
      std::cout << "Cannot set HDR control on/off" << std::endl;
      return false;
    }
    return true;
  }
}

FlyCapture::FlyCapture(int camid, CameraFamily::ID camfam,
                       FlyCapture2::VideoMode video_mode,
                       FlyCapture2::FrameRate frame_rate) {
  is_good_ = Open(camid, camfam, video_mode, frame_rate);
  if (!is_good_)
    return;
  Run();
}

FlyCapture::~FlyCapture() { Release(); }

bool FlyCapture::Open(int camid, CameraFamily::ID camfam,
                      FlyCapture2::VideoMode video_mode,
                      FlyCapture2::FrameRate frame_rate) {
  using namespace FlyCapture2;
  camera_family_ = camfam;
  BusManager busMgr;
  unsigned int ncams;
  Error error;
  if ((error = busMgr.GetNumOfCameras(&ncams)) != PGRERROR_OK) {
    error.PrintErrorTrace();
    printf("Camera enumerate");
    return false;
  }

  if (ncams == 0)
    return false;

  for (unsigned int i = 0; i < ncams; i++) {
    PGRGuid guid;
    if ((error = busMgr.GetCameraFromIndex(i, &guid)) != PGRERROR_OK) {
      error.PrintErrorTrace();
      printf("Cannot get guid");
      return false;
    }

    if (i == camid) {
      camera_ = new Camera();
      if ((error = camera_->Connect(&guid)) != PGRERROR_OK) {
        error.PrintErrorTrace();
        printf("Cannot connect guid");
        return false;
      }
      break;
    }
  }

  // Get the camera information
  CameraInfo camInfo;
  if ((error = camera_->GetCameraInfo(&camInfo)) != PGRERROR_OK) {
    error.PrintErrorTrace();
    printf("Cannot get camera info");
    return false;
  }

  PrintCameraInfo(&camInfo);

  if ((error = camera_->StartCapture()) != PGRERROR_OK) {
    error.PrintErrorTrace();
    printf("Cannot start capture");
    return false;
  }

  if ((error = camera_->SetVideoModeAndFrameRate(video_mode, frame_rate)) !=
      PGRERROR_OK) {
    error.PrintErrorTrace();
    printf("Cannot set video mode and frame rate");
    return false;
  }

  bool supported = false;
  if ((error = camera_->GetVideoModeAndFrameRateInfo(
           video_mode, frame_rate, &supported)) != PGRERROR_OK) {
    error.PrintErrorTrace();
    printf("Cannot get support info of video mode and frame rate");
    return false;
  }
  if (!supported)
    return false;

  Init();

  return true;
}

void FlyCapture::Init() {
  // TODO: get image resolution on FlyCapture2

  hdr_support_ = GenericInitializeHDR(camera_, camera_family_);
  using namespace FlyCapture2;
  using namespace std;
  // NOTE: list all necessary features needed for setup here
  map_string_paramid_[string("Brightness")] = BRIGHTNESS;
  map_string_paramid_[string("Exposure")] = AUTO_EXPOSURE;
  map_string_paramid_[string("Sharpness")] = SHARPNESS;
  map_string_paramid_[string("WhiteBalance")] = WHITE_BALANCE;
  map_string_paramid_[string("HUE")] = HUE;
  map_string_paramid_[string("Saturation")] = SATURATION;
  map_string_paramid_[string("Gamma")] = GAMMA;
  map_string_paramid_[string("Shutter")] = SHUTTER;
  map_string_paramid_[string("Gain")] = GAIN;
  map_string_paramid_[string("Iris")] = IRIS;
  map_string_paramid_[string("Focus")] = FOCUS;
  map_string_paramid_[string("Zoom")] = ZOOM;
  map_string_paramid_[string("Pan")] = PAN;
  map_string_paramid_[string("Tilt")] = TILT;
  map_string_paramid_[string("FrameRate")] = FRAME_RATE;
  Error error;

  map<string, PropertyType> map_string_paramid__temp;
  map_string_paramid__temp.swap(map_string_paramid_);
  // display camera parameters' value
  for (std::map<string, PropertyType>::iterator it = map_string_paramid_.begin();
       it != map_string_paramid_.end(); ++it) {
    std::cout << (*it).first.data() << std::endl;
    std::pair<unsigned int, unsigned int> minmax;
    PropertyInfo propinfo((*it).second);
    if ((error = camera_->GetPropertyInfo(&propinfo)) != PGRERROR_OK ||
        !propinfo.present) {
      error.PrintErrorTrace();
      cout << "Cannot get property info " << endl;
      continue;
    }
    if (propinfo.max > 0) {
      map_string_paramid_[(*it).first] = (*it).second;
      parameter_.map_paramid_minmax[(*it).first] =
          make_pair(propinfo.min, propinfo.max);
      Property prop((*it).second);
      if ((error = camera_->GetProperty(&prop)) != PGRERROR_OK) {
        error.PrintErrorTrace();
        cout << "Cannot get property " << endl;
        continue;
      }
      parameter_.map_string_paramvalue[(*it).first] = prop.valueA;
      parameter_.print_parameter((*it).first);
      if (!prop.onOff) {
        std::cout << "power:" << prop.onOff << std::endl;
        prop.onOff = true;
        camera_->SetProperty(&prop);
      }
      parameter_.map_string_auto[(*it).first] = true;
    }
  }
  controllable_ = (parameter_.map_paramid_minmax.size() > 0);
}

// read latest frame
EventPtr FlyCapture::CaptureEvent() {
  using namespace FlyCapture2;
  Error error;
  Image raw_image;
  camera_->RetrieveBuffer(&raw_image);
  // Create a converted image
  Image converted_image;
  // Convert the raw image
  if ((error = raw_image.Convert(PIXEL_FORMAT_RGB8, &converted_image)) !=
      PGRERROR_OK) {
    error.PrintErrorTrace();
    return ImageEventPtr();
  }
  cv::Mat m_temp(converted_image.GetRows(), converted_image.GetCols(), CV_8UC3,
                 (unsigned char *)converted_image.GetData());
  auto t = std::chrono::high_resolution_clock::to_time_t(
      std::chrono::high_resolution_clock::now());
  ;
  return ImageEventPtr(new ImageEvent(t, m_temp.clone()));
}

// useful method to setup HDR capturing function (works with FireFly cameras)
bool FlyCapture::SetHDR(bool on) {
  if (!hdr_support_)
    return false;
  return GenericSetHDR(on, camera_, camera_family_);
}

unsigned int FlyCapture::GetProperty(std::string const &prop_name) const {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0)
    return parameter_.map_string_paramvalue.at(prop_name);
  return 0;
}

bool FlyCapture::SetProperty(std::string const &prop_name, unsigned int value) {
  if (parameter_.map_string_paramvalue.count(prop_name) != 0) {
    if (value < parameter_.map_paramid_minmax[prop_name].first ||
        value > parameter_.map_paramid_minmax[prop_name].second)
      return false;
    FlyCapture2::Error error;
    FlyCapture2::Property prop(map_string_paramid_[prop_name]);
    prop.autoManualMode = false;
    prop.valueA = parameter_.map_string_paramvalue[prop_name];

    if ((error = camera_->SetProperty(&prop)) != FlyCapture2::PGRERROR_OK) {
      error.PrintErrorTrace();
      std::cout << "Cannot set property for " << prop_name << std::endl;
      return false;
    }
    parameter_.print_parameter(prop_name);
    parameter_.map_string_paramvalue[prop_name] = value;
    return true;
  }
}

// proper shutdown for the camera
void FlyCapture::Release() {
  is_good_ = false;
  using namespace FlyCapture2;
  Error error;
  if (camera_) {
    if ((error = camera_->StopCapture()) != PGRERROR_OK) {
      error.PrintErrorTrace();
    }
    if ((error = camera_->Disconnect()) != PGRERROR_OK) {
      error.PrintErrorTrace();
    }
  }
}

FlyCapture2::Camera &FlyCapture::GetCamera() { return *camera_; }
} // namespace sense