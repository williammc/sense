// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/sensor_replay.h"
#include "sense/tracking_events.h"

#include <fstream>
#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace sense {

static ImageEventPtr MakeImageEvent(const SenseScalar time, const std::string & file) {
  cv::Mat img = cv::imread(file);
  if (img.channels() == 3) cv::cvtColor(img, img, CV_BGR2RGB);
  return ImageEventPtr(new ImageEvent(time, img));
}

SensorReplay::SensorReplay(const std::string &filename, bool loopback, bool verbose, bool threading) {
  path_ = filename;
  full_fn_ = filename;
  loopback_ = loopback;
  verbose_ = verbose;
  threading_ = threading;
  std::string::size_type divider = filename.find_last_of('/');
  if (divider != std::string::npos) {
    path_ = path_.substr(0, divider + 1);
  } else {
    path_ = "";
  }
  ifs_.open(filename.c_str());
  if (ifs_.fail()) {
    std::cerr << "Cannot open file " << filename << "!" << "\n";
    return;
  }
  
  identity_ = full_fn_;

  if (threading_)
    Run();
}

SensorReplay::~SensorReplay() {
  if (ifs_.is_open()) ifs_.close();
  Stop();
}

EventPtr SensorReplay::CaptureEvent() {
  while (!ifs_.eof() && !stop_) {
looping:
    std::string tag;
    ifs_ >> tag;
    SenseScalar time;
    if (tag == "IMAGE") {
      std::string name;
      ifs_ >> time >> name;
      if (verbose_)
        std::cout << "loading image: " << (path_ + name) << "!" << "\n";
      return MakeImageEvent(time, path_ + name);
    } else if (tag == "INERTIAL") {
      SenseScalar time;
      std::array<float, 3> g,a,m;
      std::array<float, 4> q;
      ifs_ >> time; ifs_ >> g >> a >> m >> q;
      if (verbose_)
        std::cout << "loading inertial: " << time
           << " " << g << " " << a  << " " << q << "\n";
      return InertialEventPtr(new InertialEvent(time, g, a, m, q));
    } else if (tag == "GPS") {
      SenseScalar diffdelay;
      std::array<float, 3> pos;
      int numsat, fix;
      ifs_ >> time >> pos >> numsat >> diffdelay >> fix;
      return GPSEventPtr(new GPSEvent(time, pos, numsat, diffdelay, fix));
    } else if(tag == "LASER") {
      float distance;

      ifs_ >> time >> distance;
      return LaserEventPtr(new LaserEvent(time, distance));
    } else if(tag == "DEPTH-IMAGE") {
      float distance;

      std::string name;
      ifs_ >> time >> name;
      if (verbose_)
        std::cout << "loading image: " << (path_ + name) << "!" << "\n";
      cv::Mat img = cv::imread(path_ + name, -1);
      return DepthImageEventPtr(new DepthImageEvent(time, img));
    } else if (tag == "COLOR-DEPTH-IMAGE") {
      std::string cname, dname;
      bool aligned;
      ifs_ >> time >> cname >> dname >> aligned;
      if (verbose_)
        std::cout << "loading image: " << (path_ + cname) << "!" << "\n";
      cv::Mat cimg = cv::imread(path_ + cname, -1); 
      if (cimg.channels() != 3)
        return ColorAndDepthEventPtr();
      cv::cvtColor(cimg, cimg, CV_BGR2RGB);
      if (verbose_)
        std::cout << "loading image: " << (path_ + dname) << "!" << "\n";
      cv::Mat dimg = cv::imread(path_ + dname, -1);
      return ColorAndDepthEventPtr(new ColorAndDepthEvent(time, cimg, dimg));
    } else if (tag == "GRAVITY") {
      std::array<float, 3> g;
      ifs_ >> time >> g;
      return GravityEventPtr(new GravityEvent(time, g));
    } 
  }
  if (ifs_.eof() && loopback_ && !stop_) {
    ifs_.close();
    ifs_.open(full_fn_);
    goto looping;
  }
  return EventPtr();
}
}  // namespace sense