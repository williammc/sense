// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/event_queue.h"

namespace sense {

EventQueue::EventQueue() {}

EventQueue::~EventQueue() {
  clear();
}

void EventQueue::push(EventPtr evt) {
  std::unique_lock<std::mutex> lock(mutex_);
  queue_.push(evt);
}

EventPtr EventQueue::pop() {
  if(queue_.empty()) throw EmptyException();
  std::unique_lock<std::mutex> lock(mutex_);
  EventPtr result = queue_.top();
  queue_.pop();
  return result;
}

bool EventQueue::empty() const {
  std::unique_lock<std::mutex> lock(*(const_cast<std::mutex*>(&mutex_)));
  return queue_.empty();
}

void EventQueue::discard(SenseScalar until) {
  while (!queue_.empty() && queue_.top()->timestamp() < until) {
    std::unique_lock<std::mutex> lock(mutex_);
    EventPtr evt = queue_.top();
    queue_.pop();
  }
}

void EventQueue::clear() {
  while(!queue_.empty()){
    std::unique_lock<std::mutex> lock(mutex_);
    EventPtr evt = queue_.top();
    queue_.pop();
  }
}
}  // namespace sense