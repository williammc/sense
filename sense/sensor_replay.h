#pragma once
#include <fstream>
#include <string>
#include "sense/sense_manager.h"
#include "sense/tracking_events.h"

namespace sense {
class SensorReplay : public SensePublisher {
public:
  SensorReplay(const std::string & file, bool loopback = true, bool verbose = false, bool threading = true);
  ~SensorReplay();
  virtual EventPtr CaptureEvent();
protected:
  bool threading_;
  bool verbose_;
  bool loopback_;
  std::ifstream ifs_;
  std::string path_;
  std::string full_fn_;
};
}  // namespace sense