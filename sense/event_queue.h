// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <queue>
#include <memory>
#include <mutex>
#include "sense/datatypes.h"
#include "sense/sense_api.h"

namespace sense {
// abstract class for all events
struct Event {
  Event(SenseScalar t = 0) : timestamp_(t) {}
  virtual ~Event() {}

  SenseScalar timestamp() const {
    return timestamp_;
  }
  void timestamp(SenseScalar time) {
    timestamp_ = time;
  }
  bool operator < (const Event & other) const {
    return timestamp() < other.timestamp();
  }

 protected:
  SenseScalar timestamp_;
};

using EventPtr = std::shared_ptr<Event>;

struct LessEvent {
  inline bool operator () ( const EventPtr a, const EventPtr b ) {
    return a->timestamp() > b->timestamp();
  }
};

class EmptyException : public std::exception {
public:
  virtual const char * what() const throw() { return "EventQueue empty"; }
};

struct EventQueue {
  EventQueue();
  virtual ~EventQueue();

  virtual void push(EventPtr);
  virtual EventPtr pop();
  virtual bool empty() const;
  virtual void discard( SenseScalar until );
  virtual void clear();
  size_t size() { return queue_.size(); }

  template<typename EVENT>
  void delete_all_specific_events() {
    std::vector<EventPtr > store;
    while (!empty()) {
      EventPtr myevent = pop();
      EVENT * an_event = dynamic_cast<EVENT *>(myevent.get());
      if (an_event == NULL)
        store.push_back(myevent);
    }
    for (size_t i = 0; i < store.size(); i++) {
      push(store[i]);
    }
  }

protected:
  std::priority_queue<EventPtr, std::deque<EventPtr >, LessEvent> queue_;
  std::mutex mutex_;
};
typedef std::shared_ptr<EventQueue> EventQueuePtr;
}  // namespace sense