// Copyright 2014 The Sense Authors. All rights reserved.
#include "sense/sensor_recorder.h"

#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <thread>
#include <functional>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/tracking_events.h"
#include "sense/image_writer.h"

using namespace std;

namespace sense {

SensorRecorder::SensorRecorder(const std::string & filename) {
  recording_ = false;
  std::string::size_type divider;
  divider = filename.find_last_of('/');
  if (divider == std::string::npos) {
    divider = filename.find_last_of('\\');
  }

  output_path_ = filename.substr(0, divider);
  prefix_ = filename.substr(divider + 1);

  printf("SensorRecorder: output_path_:%s \n", output_path_.c_str());
  printf("SensorRecorder: prefix_:%s \n", prefix_.c_str());

  divider = prefix_.find_last_of('.');
  if(divider!=std::string::npos)
    prefix_ = prefix_.substr(0, divider);
  logfile_.open( filename.c_str() );
  logfile_ << std::setprecision(30);
  image_writer_ = nullptr;
  depth_writer_ = nullptr;
  fused_depth_writer_ = nullptr;
}

SensorRecorder::~SensorRecorder() {
  recording_ = false;
  WaitForWriterToEnd(&image_writer_);
  WaitForWriterToEnd(&depth_writer_);
  WaitForWriterToEnd(&fused_depth_writer_);

  logfile_.flush();
  logfile_.close();
}

void SensorRecorder::OnEventCallback(EventPtr in_evt) {
  //SenseSubscriber::OnEventCallback(in_evt);
  if (!recording_) return;
  if (std::dynamic_pointer_cast<ColorAndDepthEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<ColorAndDepthEvent>(in_evt);
    std::string color_filename, depth_filename;
    StoreImageCommon(&image_writer_, evt.frame(), output_path_, color_filename, "color", "sensor_recorder");
    StoreImageCommon(&depth_writer_, evt.depth_frame(), output_path_, depth_filename, "depth", "sensor_recorder");
    logfile_ << "COLOR-DEPTH-IMAGE" << " " << evt.timestamp()
      << " " << color_filename
      << " " << depth_filename
      << " " << evt.aligned_to_color() << "\n";
  } else if (std::dynamic_pointer_cast<ImageEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<ImageEvent>(in_evt);
    std::string filename;
    StoreImageCommon(&image_writer_, evt.frame(), output_path_, filename, "color", "sensor_recorder");
    logfile_ << "IMAGE" << " " << evt.timestamp() << " " << filename << "\n";
  } else if (std::dynamic_pointer_cast<InertialEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<InertialEvent>(in_evt);
    logfile_ << "INERTIAL " << evt.timestamp() << " " 
      << evt.gyro() << " " << evt.accel() << " " << evt.magnet() 
      << " " << evt.quaternion() << "\n";
  } else if (std::dynamic_pointer_cast<GPSEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<GPSEvent>(in_evt);
    logfile_ << "GPS " << evt.timestamp() << " " << evt.position() << " " << evt.num_sat() << " " << evt.diffdelay() << " " << evt.fix() << "\n";
  } else if (std::dynamic_pointer_cast<LaserEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<LaserEvent>(in_evt);
    logfile_ << "LASER " << evt.timestamp() << " " << evt.distance() << "\n";
  } else if (std::dynamic_pointer_cast<DepthImageEvent>(in_evt)) {

    auto& evt = *std::dynamic_pointer_cast<DepthImageEvent>(in_evt);
    std::string filename;
    StoreImageCommon(&depth_writer_, evt.frame(), output_path_, filename, "depth", "sensor_recorder");
    logfile_ << "DEPTH-IMAGE" << " " << evt.timestamp()
      << " " << filename
      << " " << evt.aligned_to_color() << "\n";
  } else if (std::dynamic_pointer_cast<GravityEvent>(in_evt)) {
    auto& evt = *std::dynamic_pointer_cast<GravityEvent>(in_evt);
    logfile_ << "GRAVITY" << " " << evt.timestamp()
      << " " << evt.gravity() << "\n";
  }
}
}  // namespace sense
