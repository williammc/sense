// Copyright 2014 The Sense Authors. All rights reserved.
#pragma once
#include <FlyCapture2.h>

#include "sense/generic_capture.h"

namespace sense {

class FlyCapture : public GenericCapture {
 public:
  FlyCapture() = delete;
  FlyCapture(const FlyCapture&) = delete;
  FlyCapture(int camid, CameraFamily::ID camfam = CameraFamily::GENERIC,
            FlyCapture2::VideoMode video_mode = FlyCapture2::VIDEOMODE_640x480Y8,
            FlyCapture2::FrameRate frame_rate = FlyCapture2::FRAMERATE_30);
  ~FlyCapture();

  bool Open(int camid, CameraFamily::ID camfam = CameraFamily::GENERIC,
            FlyCapture2::VideoMode video_mode = FlyCapture2::VIDEOMODE_640x480Y8,
            FlyCapture2::FrameRate frame_rate = FlyCapture2::FRAMERATE_30);

  void Init() override;

  //! read latest frame
  EventPtr CaptureEvent() override;

  //! useful method to setup HDR capturing function (works with FireFly cameras)
  bool SetHDR(bool on) override;

  unsigned int GetProperty(std::string const& prop_name) const override;

  bool SetProperty(std::string const& prop_name, unsigned int value) override;

  //! proper shutdown for the camera
  void Release() override;

  FlyCapture2::Camera& GetCamera();

 protected:
  unsigned frame_width_;
  unsigned frame_height_;
  CameraFamily::ID camera_family_;
  FlyCapture2::Camera* camera_;
  std::map<std::string, FlyCapture2::PropertyType> map_string_paramid_;
};
}  // namespace sense