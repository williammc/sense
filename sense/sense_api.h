// Description: import/export macros for creating DLLS with MSVC
// Use SENSE_API macro to declare it.
// Sparate macros for each DLL required.
#pragma once

#ifdef _MSC_VER  // Microsoft compiler:
  #ifdef Sense_SHARED_LIBS
    #ifdef sense_EXPORTS
      #define SENSE_API __declspec(dllexport)
    #else
      #define SENSE_API __declspec(dllimport)
    #endif
  #else
    #define SENSE_API
  #endif

#else  // not MSVC
  #define SENSE_API
#endif

