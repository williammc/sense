#include "sense/pd5x_laserdriver.h"

#include <chrono>
#include <iostream>
#include <fcntl.h>
#include <thread>
#include <windows.h>

using namespace std;

namespace sense {

// compares string with a char array
bool CompareString2CharArray(std::string const& s, char* ca) {
	for (std::string::size_type i = 0; i < s.length(); ++i) {
    if (s.at(i) != ca[i])
      return false;
  }
  return true;
}

// given a command, check if the reply is correct
bool check_command(HANDLE const& filehandle,
                  std::string const& command, std::string const& exp_reply, const int timeout = 10) {
  DWORD res = 0;
  WriteFile(filehandle, command.c_str(), command.length(), &res, NULL);
  if (res < 0) {
    cout << "Cannot write '"<< command << "' to serial port res:" << res << endl;
    return false;
  } else {
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
    string buffer;
    buffer.resize(exp_reply.length()+3);
    ReadFile(filehandle, &buffer.at(0), exp_reply.length(), &res, NULL);
    int inner_count=0;
    while (res<exp_reply.length() && inner_count < 10) {
      inner_count++;
      //boost::this_thread::sleep(boost::posix_time::millisec(static_cast<int64_t>(timeout)));
	  std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
      DWORD inner_res;
      ReadFile(filehandle, &buffer.at(res), exp_reply.length()-res, &inner_res, NULL);
      if (inner_res > 0)
        res += inner_res;
    }
    if (buffer.find_first_of(exp_reply) == string::npos) {
      printf("LaserDriver:: Oops command '%s' to serial port reply:'%s'", command.c_str(), buffer.c_str());
      return false;
    }
  }
  return true;
}

PD5xLaserdriver::~PD5xLaserdriver() {
  this->close();
  Stop();
}

void PD5xLaserdriver::close() {
  if (closed_)
    return;
  Stop();

  if (measuremode_ == MULTIPLE_EXTEND) {
    cout << "LaserDriver:: Stop Multiple Measurement Extended" << endl;
    check_command(filehandle_, "0 0 setMonVal;", "2 0 0 Reply");
    check_command(filehandle_, "8 0 setMonVal;", "2 0 8 Reply");
    check_command(filehandle_, "2 -1 -1 doMeasDistExt;", "0 0 0 0 Reply");
  } else if(measuremode_ == MULTIPLE) {
    cout << "Stop Multiple Measurement" << endl;
    check_command(filehandle_, "stopTracking;", "0 0 Reply;");
  }
  SetMeasuringMode(TARGETTING);
//  check_command(filehandle_, "switchMeasOff;", "0 0 Reply");
  CloseHandle(filehandle_);
  closed_ = true;
}

EventPtr PD5xLaserdriver::CaptureEvent() {
  if (closed_) return EventPtr();
  if (change_mode_.load()) {
    if (measuremode_ == MULTIPLE) {
      string command = "stopTracking;";  // command code for targetting
      DWORD res;
      WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
      if (res < 0) {
        cout << "Cannot write 'stopTracking;' to serial port:" << res << endl;
        return EventPtr();
      }
    }
    measuremode_ = new_mode_;
    b_needreset_ = true;
    change_mode_.store(false);
  }

  double d_timeout = 10 * 1.e-3;  // convert to micro-seconds (as in ScopedTimer)
  auto start_time = std::chrono::steady_clock::now();
  DWORD res = 0;
  if (b_needreset_) {
    // Specify a set of events to be monitored for the port.
    SetCommMask (filehandle_, EV_RXCHAR | EV_CTS | EV_DSR | EV_RLSD | EV_RING);
    PurgeComm(filehandle_,NULL); // Empty buffer
  }
  string command = "";
  if (measuremode_ == TARGETTING) {
    cout << " Targetting" << endl;
    b_needreset_ = true;
  } else if (measuremode_ == SINGLE) {
    cout << "Single Measurement" << endl;
    command = "1 doMeasDist;";  // command code for single measurement
    WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
    b_needreset_ = true;
  } else if(measuremode_ == MULTIPLE) {
    if (b_needreset_) {
      b_needreset_ = false;
      cout << "Multiple Measurement" << endl;
      command = "1 startTracking;"; // command code for multiple measurement
      WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
      if (res < 0) {
        cout << "Cannot write '1 startTracking;' to serial port res:" << res << endl;
        return LaserEventPtr();
      } else {
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        ReadFile(filehandle_, ca_buffer_, 10, &res, NULL);
      }
    }
  } else if (measuremode_ == SINGLE_EXTEND) {
    cout << "LaserDriver:: Single Measurement Extended" << endl;
    command = "0 -1 -1 doMeasDistExt;"; // command code for single measurement
    WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
  } else if (measuremode_ == MULTIPLE_EXTEND) {
    if (b_needreset_) {
      b_needreset_=false;
      cout << "LaserDriver:: Multiple Measurement Extended" << endl;
      command = "1 -1 20 doMeasDistExt;"; // command code for multiple measurement
      WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
      if(res < 0) {
        cout << "LaserDriver:: Cannot write '1 -1 -1 doMeasDistExt;' to serial port res:" << res << endl;
        return LaserEventPtr();
      } else {
        if(!check_command(filehandle_, "0 1 setMonVal;", "\n2 0 0 Reply"))
          return LaserEventPtr();
        if(!check_command(filehandle_, "8 1 setMonVal;", "\n2 0 8 Reply"))
          return LaserEventPtr();
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        ReadFile(filehandle_, &ca_buffer_, 10, &res, NULL);
      }
    }
  }

  if (b_needreset_)
    cout << " writing to serial port res:" << res << endl;
  if (cached_bytes_.size() > 0) {
    if (measuremode_ == SINGLE || measuremode_ == SINGLE_EXTEND) {
      cached_bytes_.clear();
    } else if (measuremode_ == MULTIPLE || measuremode_ == MULTIPLE_EXTEND) {
      while (!cached_bytes_.empty()) {
        cached_bytes_.erase(cached_bytes_.begin());
        if (!cached_bytes_.empty()) {
          if (cached_bytes_.at(0) == 'M')
            break;
        }
      }
    }
  }

  // read distance result
  float laser_dist = 0;
  res = -1;
  int iter = 0;
  auto current_time = start_time;
  bool keep_checking = true;
  int error_code = -1;
  int scanf_return = 0;
  while (keep_checking) {
	  current_time = std::chrono::steady_clock::now();
	  if (d_timeout > 0 && std::chrono::duration_cast<std::chrono::microseconds>(current_time- start_time).count() > d_timeout)
      break;
    keep_checking++;
    // read bytes (maximum possible)
    ReadFile(filehandle_, &ca_buffer_, max_readout_bytes, &res, NULL);
    if (res > 0) {
      iter++;
      for (DWORD i = 0; i < res; ++i)
        cached_bytes_.push_back(ca_buffer_[i]);
      bool pattern_ok = false;
      if (measuremode_ == SINGLE) {
        scanf_return = sscanf((char*)&cached_bytes_.front(),"2 %d %f Reply", &error_code, &laser_dist);
        pattern_ok = scanf_return == 2;
      } else if(measuremode_ == SINGLE_EXTEND) {
        //Reply: '2 0 1844364 324 Reply'. 2nd number is error code, 3rd number is distance in um, 4th number is measurement signal
        scanf_return = sscanf((char*)&cached_bytes_.front(), "2 %d %f %f Reply", &error_code, &laser_dist);
        laser_dist *= 1.e-6f;
        pattern_ok = (scanf_return == 2);
      } else if(measuremode_ == MULTIPLE) {
        if (cached_bytes_[0] != 'M')
          return LaserEventPtr();
        char temp1, temp2;
        scanf_return = sscanf((char*)&cached_bytes_.front(),"M: 8 %d 0 %f%cM%c",
                              &error_code, &laser_dist, &temp1, &temp2);
        pattern_ok = (scanf_return == 4);
      } else if(measuremode_ == MULTIPLE_EXTEND) {
        if (cached_bytes_[0] != 'M')
          return LaserEventPtr();
        char temp1, temp2;
        scanf_return = sscanf((char*)&cached_bytes_.front(), "M: 8 %d 0 %f%cM%c",
                              &error_code, &laser_dist, &temp1, &temp2);
        laser_dist *= 1.e-6f;
        pattern_ok = (scanf_return == 4);
      }
      if (pattern_ok || iter >= 3)
        keep_checking = false;
    }
    if(keep_checking)
	   std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }

  if (laser_dist  <= 0.0 || error_code != 0) {
    cout << "===================" << endl;
    cout << "LaserDriver:: Cannot read Distance from serial port res:" << res << endl;
    cout << "distance:" << laser_dist
         << " error_code:" << error_code
         << " scanf_return:" << scanf_return
         << " iter:" << iter << endl;
	for (std::vector<char>::size_type i = 0; i < cached_bytes_.size(); ++i) {
      if ((i > 0) && (cached_bytes_[i] == 'M'))
        break;
      cout << cached_bytes_[i];
    }
    cout << "===================" << endl;
    return LaserEventPtr();
  }
  auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  return LaserEventPtr(new LaserEvent(t, laser_dist));
}

bool PD5xLaserdriver::OpenAndConfig(std::string s_portpath) {
  cout << " Opening serial port:" << s_portpath << endl;
  // open serial port
  filehandle_ = CreateFileA(s_portpath.c_str(),GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
  if (filehandle_ == INVALID_HANDLE_VALUE) {
    cout  << "INFO:: Could not open port for Laser" << endl;
    return false;
  }

  // configure serial port
  DCB dcb;
  GetCommState(filehandle_, &dcb);
  dcb.BaudRate = 115200;
  dcb.ByteSize = 8;
  dcb.StopBits = ONESTOPBIT;
  dcb.Parity = NOPARITY;

  if (!::SetCommState(filehandle_, &dcb)) {
    cout << "ERROR:: Could not change parameters for comport" << endl;
    return false;
  }

  COMMTIMEOUTS timeouts;

  cout << " GetCommTimeouts (filehandle_, &timeouts): " << GetCommTimeouts (filehandle_, &timeouts) << endl;

  const int timeout = 0;
  if (timeout < 0) {
    // Settings for infinite timeout.
    timeouts.ReadIntervalTimeout = 0;
    timeouts.ReadTotalTimeoutMultiplier = 0;
    timeouts.ReadTotalTimeoutConstant = 0;
  } else if (timeout == 0) {
    // Return immediately if no data in the input buffer.
    timeouts.ReadIntervalTimeout = MAXDWORD;
    timeouts.ReadTotalTimeoutMultiplier = 0;
    timeouts.ReadTotalTimeoutConstant = 0;
  } else {
    // Wait for specified timeout for char to arrive before returning.
    timeouts.ReadIntervalTimeout = MAXDWORD;
    timeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
    timeouts.ReadTotalTimeoutConstant = timeout;
  }
  if (SetCommTimeouts(filehandle_,&timeouts) != 0)
    cout << "Could not set timeouts" << endl;

  std::cout << " FileHandle:" << filehandle_ << std::endl;

  cout << " wakes up the laser device ..." << endl;
  string command = ";"; // command code for targetting
  DWORD res;
  WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
  if (res < 0) {
    cout << "Cannot write ';' to serial port:" << res << endl;
    return false;
  } else {
    ReadFile(filehandle_, &ca_buffer_, 5, &res, NULL);
  }

  command = "ping;";  // command code for targetting
  WriteFile(filehandle_, command.c_str(), command.length(), &res, NULL);
  if (res < 0) {
    cout << "Cannot write 'ping;' to serial port:" << res << endl;
    return false;
  } else {
    ReadFile(filehandle_, &ca_buffer_, 10, &res, NULL);
  }
  return true;
}

bool PD5xLaserdriver::SetMeasuringMode(MeasuringMode mode) {
  if (mode == measuremode_) {
    return false;
  }
  new_mode_ = mode;
  change_mode_.store(true);
  return true;
}
}  // namespace sense
