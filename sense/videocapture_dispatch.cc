#include "sense/videocapture_dispatch.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "sense/tracking_events.h"

namespace sense {

VideoCaptureDispatch::VideoCaptureDispatch(int device, CameraFamily::ID camfam,
                                           std::string config_file)
    : camfam_(camfam) {
  auto open = [&]() -> bool {
    sourcetype = LIVE;
    //try with DC1394Capture first
#ifdef SENSE_HAVE_DC1394_2
    genericcapture =
        std::shared_ptr<GenericCapture>(new DC1394Capture(device, camfam_));
    if (genericcapture->IsGood())
      return true;
#endif
#ifdef SENSE_HAVE_FLYCAPTURE2
    genericcapture =
        std::shared_ptr<GenericCapture>(new FlyCapture(device, camfam_));
    if (genericcapture->IsGood())
      return true;
#endif
#ifdef SENSE_HAVE_UEYE
    genericcapture = std::shared_ptr<GenericCapture>(
        new UEyeCapture(config_file, device, camfam_));
    if (genericcapture->IsGood())
      return true;
#endif
    genericcapture =
        std::shared_ptr<GenericCapture>(new GenericCapture(device, camfam_));
    if (genericcapture->IsGood())
      return true;
    return false;
  };

  if (!open()) {
    std::cerr << "VideoCaptureDispatch: cannot camera #" << device << std::endl;
    return;
  }

  identity_ = std::string("Camera_") + std::to_string(device);
  Run();
}

VideoCaptureDispatch::VideoCaptureDispatch(std::string filename) {
  auto open = [&]() {
    if (filename.substr(filename.length() - 3, 3) != "avi") {
      video_source = filename;
      char ca_temp[200];
      if (sprintf(ca_temp, filename.c_str(), image_sequence_index) > 0) {
        sourcetype = IMAGE_SEQUENCE;
        image_sequence_index = 0;
        sprintf(ca_temp, filename.c_str(), image_sequence_index);
      } else {
        sourcetype = IMAGE;
        sprintf(ca_temp, filename.c_str(), 0);
      }
      std::fstream fs(ca_temp, std::ios_base::in);
      if (fs.is_open()) {
        fs.close();
        return true;
      }
      return false;
    } else {
      sourcetype = VIDEO;
      genericcapture =
          std::shared_ptr<GenericCapture>(new GenericCapture(filename));
      if (genericcapture->IsGood())
        return true;
      return false;
    }
  };

  if (!open()) {
    std::cerr << "VideoCaptureDispatch: cannot open file from " << filename
              << std::endl;
    return;
  }

  identity_ = filename;
  Run();
}

VideoCaptureDispatch::~VideoCaptureDispatch() {
  if (genericcapture) {
    genericcapture->release();
    genericcapture.reset();
  }
}

void VideoCaptureDispatch::SetProperty(std::string const &param,
                                       unsigned int value) {
  genericcapture->SetProperty(param, value);
}

unsigned int VideoCaptureDispatch::GetProperty(std::string const &param) {
  return genericcapture->GetProperty(param);
}

bool VideoCaptureDispatch::SetHDR(bool on) {
  return genericcapture->SetHDR(on);
}

// Returns the next frame from the buffer.
// This function blocks until a frame is ready.
EventPtr VideoCaptureDispatch::CaptureEvent() {
  if (!genericcapture)
    return EventPtr();
  cv::Mat m_temp, m_temp_bgr;
  switch (sourcetype) {
  case IMAGE:
    m_temp_bgr = cv::imread(video_source);
    cv::cvtColor(m_temp_bgr, m_temp, cv::COLOR_BGR2RGB);
    break;
  case IMAGE_SEQUENCE:
    char ca_temp[200];
    sprintf(ca_temp, video_source.c_str(), ++image_sequence_index);
    m_temp_bgr = cv::imread(ca_temp);
    if (m_temp_bgr.empty()) {
      image_sequence_index = 0;
      sprintf(ca_temp, video_source.c_str(), ++image_sequence_index);
      m_temp_bgr = cv::imread(ca_temp);
    }
    cv::cvtColor(m_temp_bgr, m_temp, cv::COLOR_BGR2RGB);
    break;
  case VIDEO:
  case LIVE:
    return genericcapture->CaptureEvent();
    break;
  default:
    ;
  }
  if (m_temp.empty())
    return EventPtr();
  auto t = sense::Timestamp();
  return ImageEventPtr(new ImageEvent(t, m_temp));
}

GenericCapture *VideoCaptureDispatch::GetVideoCapture() {
  return genericcapture.get();
}

GenericParameter &VideoCaptureDispatch::GetParameter() {
  return genericcapture->parameter();
}
} // namespace sense
