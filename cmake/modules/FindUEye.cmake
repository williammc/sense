# - Find UEye
# This module finds if the UEye SDK inlcudes and libraries are installed
#
# This module sets the following variables:
#
# UEYE_FOUND
#    True if the UEye SDK includes and libraries were found
# UEYE_INCLUDE_DIR
#    The include path of the uEye.h header file
# UEYE_LIBRARIES
#    The location of the library files

# check for 32 or 64bit
if(NOT WIN32 AND NOT APPLE)
  EXEC_PROGRAM(uname ARGS -m OUTPUT_VARIABLE CMAKE_CUR_PLATFORM)
  if( CMAKE_CUR_PLATFORM MATCHES "x86_64")
    set( HAVE_64_BIT 1 )
  else()
    set( HAVE_64_BIT 0 )
  endif()
else()
  if(CMAKE_CL_64)
    set( HAVE_64_BIT 1 )
  else()
    set( HAVE_64_BIT 0 )
  endif()
endif()

# set possible library paths depending on the platform architecture.
if(HAVE_64_BIT)
  set(CMAKE_LIB_ARCH_APPENDIX 64)
  set(UEYE_POSSIBLE_LIB_DIRS "lib64" "lib" "bin")
  message( STATUS "FOUND 64 BIT SYSTEM")
else()
  set(CMAKE_LIB_ARCH_APPENDIX 32)
  set(UEYE_POSSIBLE_LIB_DIRS "lib" "Lib")
  message( STATUS "FOUND 32 BIT SYSTEM")
endif()

if(UEYE_ROOT)
  # FIND THE uEye++ include path
  find_path(UEYE_INCLUDE_DIR uEye.h
    # Windows:
    "C:/Program Files/IDS/uEye/Develop/include"
    ${UEYE_ROOT}
    "$ENV{UEYE_ROOT}/include"
    # Linux
    "/usr/include/"
    "/usr/include/ueye/"
    "$ENV{VMLibraries_DIR}/extern/linux/include"
  )
else()
  find_path(UEYE_ROOT include/uEye.h
    # Windows:
    "C:/Program Files/IDS/uEye/Develop"
    ${UEYE_ROOT}
    "$ENV{UEYE_ROOT}"
    # Linux
    "/usr"
    "/usr/local"
    "$ENV{VMLibraries_DIR}/extern/linux/include"
  )
  set(UEYE_INCLUDE_DIR "${UEYE_ROOT}/include")
endif()

if(HAVE_64_BIT)
  set(UEYE_LIB_NAMES uEye_api_64 uEye_tools_64)
else()
  set(UEYE_LIB_NAMES uEye_api uEye_tools)
endif()

find_library(UEYE_LIBRARIES NAMES ${UEYE_LIB_NAMES}
    PATHS
    "/usr/lib${CMAKE_LIB_ARCH_APPENDIX}"
    "$ENV{VMLibraries_DIR}/extern/win${CMAKE_LIB_ARCH_APPENDIX}"
    "C:/Program Files/IDS/uEye/Develop"
    "$ENV{UEYE_ROOT}"
    "$ENV{VMLibraries_DIR}/extern/linux/lib${CMAKE_LIB_ARCH_APPENDIX}"
    PATH_SUFFIXES ${UEYE_POSSIBLE_LIB_DIRS}
)

IF(UEYE_INCLUDE_DIR AND UEYE_LIBRARIES)
    SET(UEYE_FOUND true)
ENDIF()

IF(UEYE_FOUND)
    IF(NOT UEYE_FIND_QUIETLY)
        MESSAGE(STATUS "Found UEye: ${UEYE_LIBRARIES}")
    ENDIF(NOT UEYE_FIND_QUIETLY)
ELSE(UEYE_FOUND)
    IF(UEYE_FIND_REQUIRED)
        MESSAGE(FATAL_ERROR "Could not find the UEye library")
    ENDIF(UEYE_FIND_REQUIRED)
ENDIF(UEYE_FOUND)
